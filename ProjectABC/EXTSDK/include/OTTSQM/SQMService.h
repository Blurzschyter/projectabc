//
//  SQMService.h
//  OTTSQM
//
//  Created by OTT on 2017/5/15.
//  Copyright © Huawei Software Technologies Co., Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol SQMLog;
@protocol SQMHelper;
@protocol SQMPlayHelper;
@protocol SQMMediaService;
@protocol SQMUIService;
@protocol SQMDataPick;

typedef NS_ENUM(NSInteger, SQMRegisteType){
    SQMRegisteUsernamePassword,     // username+password
    SQMRegisteSessionToken          // sessionid+token
};

/**
 @protocol SQMServiceDelegate
 @brief SQMService代理协议，需要实现者实现相应的回调方法
 */
@protocol SQMServiceDelegate <NSObject>

/**
 @brief 处理已上报的crash文件
 @param fileName 已上报的crash文件
 */
- (void)handleReportedCrashFile:(NSString *)fileName;
@end

/**
 @class SQMService
 @brief SQM服务的主类，主要实现SQM上报服务，包括周期上报、实时上报、错误上报、告警上报、Crash日志上报等
 */
@interface SQMService : NSObject

/**
 The SQMLog delegate.
 */
@property (nonatomic, strong) id<SQMLog> log;

/**
 The SQMHelper delegate.
 */
@property (nonatomic, strong) id<SQMHelper> helper;

/**
 The SQMPlayHelper delegate.
 */
@property (nonatomic, strong) id<SQMPlayHelper> playHelper;

/**
 The SQMServiceDelegate delegate.
 */
@property(nonatomic, weak) id<SQMServiceDelegate> delegate;

/**
 If report is switched on by user. YES by default.
 */
@property (nonatomic) BOOL reportSwitch;

/**
 Result of SQM register.

 @return If register succeeded.
 */
- (BOOL)isRegisterSuccessful;

/**
 Setup SQMService's shared instance. Must be called before any call to shareInstance.

 @param log The SQMLog delegate.
 @param helper The SQMHelper delegate.
 @param playHelper The SQMPlayHelper delegate.
 @return The shared instance of SQMService.
 */
+ (instancetype)setupWithLog:(id<SQMLog>)log helper:(id<SQMHelper>)helper playHelper:(id<SQMPlayHelper>)playHelper;

/**
 Fetch the shared instance of SQMService, to which all interface calls should be forwarded.

 @return The shared instance of SQMService.
 */
+ (instancetype)shareInstance;

/**
 @brief 获取实现SQMMediaService协议的对象实例
 @return 实现SQMMediaService协议的对象
 */
- (id<SQMMediaService>)getMediaService;

/**
 @brief 获取UI Service对象
 @return UI对象
 */
- (id<SQMUIService>)getUIService;

/**
 @brief 获取SQM数据采集类
 @return 返回SQM数据采集类
 */
- (id<SQMDataPick>)getDataPick;

/**
 Initialize some basic services, e.g. passing through error event report.
 */
- (void)initializeService;

/**
 @brief sqm 注册
 @param sqmUrl sqmf服务地址
 @param userName 用户名
 @param key 密码
 */
- (void)registe:(NSString *)sqmUrl userName:(NSString *)userName key:(NSString *)key type:(SQMRegisteType)type callback:(void (^)(BOOL isSucceed))callback;

/**
 @brief 释放SQM
 */
- (void)unregiste NS_DEPRECATED_IOS(8_0, 8_0, "This method does nothing now. You no longer need to call it.");

@end
