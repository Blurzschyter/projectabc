//
//  SQMHelper.h
//  OTTSQM
//
//  Created by OTT on 2017/5/15.
//  Copyright © Huawei Software Technologies Co., Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 AccessType
 */
typedef NS_ENUM(NSInteger, AccessType) {
    AccessTypeNoNetwork = 0,
    AccessTypeEthernet = 1,
    AccessTypeWiFI,
    AccessType3G,
    AccessTypeLTE,
};

/**
 @class CrashLog
 @brief Crash日志收集信息，包括log file、crash time、crash cause，crash detail，sqm上传crash后，通过SQMService的delegate handleReportedCrashFile方法回调，应用决定是否删除本地的crash
 */
@interface CrashLog : NSObject
/**
 @brief crashLog文件的全路径
 */
@property(nonatomic, strong) NSString *crashLogFile;

/**
 @brief 订户id
 */
@property(nonatomic, strong) NSString *subscriberID;

/**
 @brief crash产生的时间
 */
@property(nonatomic, strong) NSString *crashTime;

/**
 @brief crash产生的原因
 */
@property(nonatomic, strong) NSString *crashCause;

/**
 @brief crash产生时的开机时间
 */
@property(nonatomic, strong) NSString *bootTime;

/**
 @brief crash产生的细节，即crash文件
 */
@property(nonatomic, strong) NSString *crashDetail;
@end

/**
 @class LocationInfo
 @brief 位置信息类
 */
@interface LocationInfo : NSObject
/**
 @brief Administrative code
 */
@property(nonatomic, strong) NSString *adCode;

/**
 @brief Location Aera Code
 */
@property(nonatomic, strong) NSString *locationAeraCode;

/**
 @brief Cell Identity
 */
@property(nonatomic, strong) NSString *cellIdentity;

/**
 @brief GPS Longitude
 */
@property(nonatomic, strong) NSString *gpsLongitude;

/**
 @brief GPS Latitude
 */
@property(nonatomic, strong) NSString *gpsLatitude;

/**
 @brief IMSI
 */
@property(nonatomic, strong) NSString *imsi;

/**
 @brief APN
 */
@property(nonatomic, strong) NSString *apn;

/**
 @brief District
 */
@property(nonatomic, strong) NSString *district;

/**
 @brief City
 */
@property(nonatomic, strong) NSString *city;

/**
 @brief City Code
 */
@property(nonatomic, strong) NSString *cityCode;
@end

/**
 @protocol SQMHelper
 @brief SQM帮助类，主要提供设备id、设备类型、ntp时间等基础数据获取
 */
@protocol SQMHelper <NSObject>
@required
/**
 @brief 获取CA证书
 @return CA证书路径
 */
- (NSString *)getCAFile;

/**
 @brief 设备ID，作为终端的唯一标识
 @return 设备id
 */
- (NSString *)getDeviceId;

/**
 @brief 获取逻辑设备id,为vsp返回的deviceid
 @return 逻辑设备id
 */
- (NSString *)getLogicalDeviceID;

/**
 @brief 设备类型，包括：iPhone,iPad
 @return 设备类型
 */
- (NSString *)getDeviceType;

/**
 @brief 会话的session id
 @return 返回会话id
 */
- (NSString *)getSessionID;

/**
 @brief 订户ID
 @return 订户ID
 */
- (NSString *)getSubscriberId;

/**
 @brief 设备型号，例如：MR303A, iphone4s
 @return 设备型号
 */
- (NSString *)getDeviceModel;

/**
 @brief 获取设备尺寸，例如：iPad air2： 9.7
 @return 返回设备尺寸
 */
- (NSNumber *) getScreemSize;

/**
 @brief 客户端版本号
 @return 客户端版本号
 */
- (NSString *)getClientVersion;

/**
 @brief 获取接入方式
 @return 接入方式
 */
- (AccessType)getAccessType;

/**
 @brief NTP服务器地址
 @return NTP服务器地址
 */
- (NSString *)getNTPServer;

/**
 @brief EPG 服务器IP地址
 @return EPG 服务器IP地址
 */
- (NSString *)getEPGIP;

/**
 @brief 认证服务器地址，例如https://x.x.x.x:port/VSP/V3/Authenticate
 @return 认证服务器地址
 */
- (NSString *)getAuthURL;

/**
 @brief 获取RRS address
 @return 返回RRS address
 */
- (NSString *)getRRSAddr;

/**
 @brief 同步NTP时间的，返回距1970年到现在的毫秒数
 @return 距1970年的毫秒数
 */
- (NSString *)milliSeconds;

/**
 @brief 获取开始时间的开始
 @return 开始时间的毫秒数
 */
- (NSNumber *)getTimeOfLastBoot;

/**
 @brief 获取crash日志列表
 @return crash日志文件列表
 */
- (NSArray<CrashLog *>*)getCrashLogs;

/**
 @brief 获取终端位置相关的信息，位置信息包括行政区域、位置区码、小区标识、GPS经度、GPS纬度等
 @return 返回终端位置信息
 */
- (LocationInfo *)getLocationInfo;

@end

@interface SQMHelper : NSObject <SQMHelper>
@end
