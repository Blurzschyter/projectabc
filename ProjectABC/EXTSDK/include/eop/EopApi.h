﻿/***************************************************************************

project :  (E)nhanced  (O)ffline   (P)layer


 _______   ____   ____
 | |_____ /  _ \ |  _ \
 |  \___||  (_) \| |_) |
 | |___  \      /|  _ /
 \_____|  \_ _ / |_|


****************************************************************************/
/*
 * @file                OfflinePlayerWin32Interface.h
 * @brief               eop extern interface
 * @date                2014/11/14
 * @version 1.0         Copyright 2014 huawei. All rights reserved.
 */


#ifndef __EOP_API_H
#define __EOP_API_H

#ifdef __cplusplus
extern "C" {
#endif


/**
 * @brief   Initialize the resources of eop
 *
 * @param none
 *
 * @return  OfflineContentManager object if successful, otherwise NULL
 */
EXPORT_C void *EopInit();


/**
 * @brief   release the resources of eop
 *
 * @param[in]   OfflineContentManager object
 *
 * @return  none
 */
EXPORT_C void EopRelease(void **offline_manager);

/**
 * @brief   set option of eop
 *
 * @param[in] offline_manager      offline manager object
 * @param[in] option      EOP_OPT define in EopPubdefine.h
 * @param[in] option_value    the value of option
 * @param[in] user_data    user data key ,this business is content_id
 *
 * @return  return  0 if successful, otherwise -1
 */
EXPORT_C INT32 EopSetOption(void *offline_manager, EOP_OPT option, void *option_value,void* user_data);


/**
 * @brief   get option of eop
 *
 * @param[in] offline_manager      offline manager object
 * @param[in] option      EOP_OPT define in EopPubdefine.h
 * @param[in] option_value    the value of option point
 * @param[in] user_data    user data key ,this business is content_id，if multiple parameter,using ":" split
 *
 * @return  return  0 if successful, otherwise -1
 */
EXPORT_C INT32 EopGetOption(void *offline_manager, EOP_OPT option, void *option_value,void* user_data);

/**
* @brief   get chunk options of eop
*
* @param[in] offline_manager      offline manager object
* @param[in] content_id         content id
*                               Opt1: NULL or "", means get tasks (which match "UserInfoFilter" parameter in getOptsJson) specific options
*                               Opt2: content id, means get this task's specific options, ignore "UserInfoFilter" parameter in getOptsJson
* @param[in] getOptsJson        get options
*                               // input example:
*                               //      {"GetChunkOpts":{"Opts":["Status", "Speed", "Progress", "DownloadedBytes", "CachedVideoTime", "config1Name", "config2Name"],
*                                        "UserInfoFilter":{"key1":"value1", "key2":"value2"}}}
* @param[out] getOptsResultJson    get chunk options result
*                               // result example:
*                               //      {"GetChunkOptsResult":
*                                           ["TaskOpts":{
*                                               "ContentId":"11-22-33-44",
*                                               "Status":1, "Speed":1021.1, "Progress":20, "DownloadedBytes":154323,
*                                               "CachedVideoTime":50, "config1Name":"value1", "config2Name":"value2"}]}
*
* @return  return  0 if successful, otherwise -1
* @warning need call DmpFree() to free getOptsResultJson memory
*/
EXPORT_C INT32 EopGetChunkOptions(void *offline_manager, const CHAR *content_id, const CHAR *getOptsJson, CHAR**getOptsResultJson);

/**
 * @brief   start  eop download
 *
 * @param[in] offline_manager      offline manager object
 * @param[in] download_url     download url
  * @param[out] content_id     content id to return,,warning :need caller to call dmpfree() to free memory
 *
 * @return  return  0 if successful, otherwise -1
 */

EXPORT_C INT32 EopStart(void *offline_manager,const CHAR* download_url,CHAR **content_id);

/**
 * @brief   pause  eop download
 *
 * @param[in] offline_manager      offline manager object
 * @param[in] content_id
 *
 * @return  return  0 if successful, otherwise -1
 */

EXPORT_C INT32 EopPause(void *offline_manager,const CHAR* content_id);


/**
 * @brief   resume  eop download
 *
 * @param[in] offline_manager      offline manager object
 * @param[in] content_id     content id
 *
 * @return  return  0 if successful, otherwise -1
 */

EXPORT_C INT32 EopResume(void *offline_manager,const CHAR* content_id);

/**
 * @brief   delete  eop download
 *
 * @param[in] offline_manager      offline manager object
 * @param[in] content_id     content id
 *
 * @return  return  0 if successful, otherwise -1
 */

EXPORT_C INT32 EopDelete(void *offline_manager,const CHAR* content_id);


/**
* @brief   get download task list
*
* @param[in] offline_manager      offline manager object
* @param[out] content_id_list     content id list string ,warning :need caller to call dmpfree() to free memory
*
* @return  return  0 if successful, otherwise -1
*/

EXPORT_C INT32 EopGetTaskList(void *offline_manager,CHAR** content_id_list);

/**
* @brief   get filterd download task list
*
* @param[in] offline_manager      offline manager object
* @param[in] filter               filter, json string
* @param[out] content_id_list     content id list string ,
*
* @return  return  0 if successful, otherwise -1
* @warning need call DmpFree() to free content_id_list memory
*/
EXPORT_C INT32 EopGetFilterTaskList(void *offline_manager,const CHAR *filter, CHAR** content_id_list);


/**
 * @brief   play local streaming
 *
 * @param[in] offline_manager      offline manager object
 * @param[in] content_id     content id
 * @param[in] local_play_url     return the local player url
 * @return  return  0 if successful, otherwise -1
 */

EXPORT_C INT32 EopPlay(void *offline_manager,const CHAR* content_id,CHAR **local_play_url);

/**
* @brief   refresh local playready CA
*
* @param[in] offline_manager      offline manager object
* @return  VOID
*/
EXPORT_C VOID EopRefreshCA(void *offline_manager);

/**
* @brief   refresh local playready CA by contentId
*
* @param[in] offline_manager      offline manager object
* @param[in] content_id           content id
* @return  return 0 if success , otherwise -1
*/
EXPORT_C INT32 EopRefreshContentCA(void *offline_manager, const CHAR* content_id);

/**
* @brief   eop sqm init
*
* @param[in] offline_manager      offline manager object
* @return  return 0 if successful, otherwise -1
*/
EXPORT_C INT32 EopSqmInit(void *offline_manager);

/**
* @brief   switch on/off eop sqm statistic
*
* @param[in] offline_manager      offline manager object
* @param[out] download_play_info  json string represent all download play info
* @return  return 0 if successful, otherwise -1
*
* @warning: need call DmpFree() to free download_play_info memory
*/
EXPORT_C INT32 EopSqmGetDownloadPlayInfo(void *offline_manager, CHAR **download_play_info);

#ifdef __cplusplus

}
#endif

#endif
