/********************************************************************
 * Copyright(C) Huawei Tech. Co., Ltd.
 * 文 件 名:    DmpSbox.h
 * 作    者:    huxiaoxiang 00160924
 * 创建日期:    2015-11-23
 * 文件描述:    DMP平台安全存储SBOX对外接口定义
 ********************************************************************/

#ifndef __DMPSBOX__
#define __DMPSBOX__


#ifdef __cplusplus
extern "C"
{
#endif

// SBOX文件类型定义
typedef VOID SBOX_FILE;


// SBOX函数接口代码混淆
#define DmpSboxEraseFile 			CE9CEB8F_752D_4174_88B4_A1F0F04F7D7C
#define DmpSboxGetFileList 			FABF7A8A_2A9C_4d5a_A1FD_B58565841F5C
#define DmpSboxGetFileSizeByName 	F495C462_FA33_4e71_9F4D_A0EFA2E49BE1
#define DmpSboxOpenFile 			CBC3918E_9AF1_47fd_9E3D_354A4A4EF190
#define DmpSboxGetFileSizeByHandle 	EF0748DB_8ECA_4f4f_93CC_F090772C26B9
#define DmpSboxReadFile 			F6CC6C4F_727F_4649_8141_D90049C6229E
#define DmpSboxWriteFile 			ABEA9FF3_33DD_4869_9669_F4AD484697C6
#define DmpSboxSeekFile 			A9226081_B1C2_4af8_A2FF_4EBA69DA85DB
#define DmpSboxGetFilePos 			FB0FD273_0A78_4d20_8671_6E7E218EC155
#define DmpSboxFlushFile 			DEC8D5C0_0795_49f3_8C79_C3291518DBC5
#define DmpSboxCloseFile 			CD738A20_440C_4d24_A6D8_AF5F592BD31E



/********************************************************************
 * 函 数 名:  DmpSboxEraseFile
 * 描    述:  安全存储内文件删除接口
 * 输入参数:  file_name     文件名
 * 输出参数:  无
 * 返 回 值:  DMP_OK    成功
              其他值    错误码
 ********************************************************************/
DMP_API INT32 DmpSboxEraseFile(const CHAR *file_name);

/********************************************************************
 * 函 数 名:  DmpSboxGetFileList
 * 描    述:  安全存储内文件列表接口
 * 输入参数:  无
 * 输出参数:  无
 * 返 回 值:  以"|"分割的文件列表，使用完毕后必须使用DmpFree接口释放
 ********************************************************************/
DMP_API CHAR * DmpSboxGetFileList();

/********************************************************************
 * 函 数 名:  DmpSboxGetFileSizeByName
 * 描    述:  安全存储内文件大小查询接口
 * 输入参数:  file_name     文件名
 * 输出参数:  无
 * 返 回 值:  文件当前大小，负数表示错误码
 ********************************************************************/
DMP_API INT32 DmpSboxGetFileSizeByName(const CHAR *file_name);

/********************************************************************
 * 函 数 名:  DmpSboxGetFileSizeByHandle
 * 描    述:  安全存储内文件大小查询接口
 * 输入参数:  sbox_file     SBOX文件句柄
 * 输出参数:  无
 * 返 回 值:  文件当前大小，负数表示错误码
 ********************************************************************/
DMP_API INT32 DmpSboxGetFileSizeByHandle(SBOX_FILE *sbox_file);

/********************************************************************
 * 函 数 名:  DmpSboxOpenFile
 * 描    述:  安全存储内文件打开接口
 * 输入参数:  file_name     文件名
              mode          打开模式，FILE_READ/FILE_WRITE/FILE_CREATE
 * 输出参数:  无
 * 返 回 值:  SBOX文件句柄
 ********************************************************************/
DMP_API SBOX_FILE * DmpSboxOpenFile(const CHAR *file_name, INT32 flags);

/********************************************************************
 * 函 数 名:  DmpSboxReadFile
 * 描    述:  安全存储内文件读取接口
 * 输入参数:  sbox_file     SBOX文件句柄
              buff          缓冲区
              size          读取大小
 * 输出参数:  无
 * 返 回 值:  实际读取字节数，负数表示错误码
 ********************************************************************/
DMP_API INT32 DmpSboxReadFile(SBOX_FILE *sbox_file, VOID *buff, INT32 size);


/********************************************************************
 * 函 数 名:  DmpSboxWriteFile
 * 描    述:  安全存储内文件写入接口
 * 输入参数:  sbox_file     SBOX文件句柄
              buff          缓冲区
              size          写入大小
 * 输出参数:  无
 * 返 回 值:  实际写入字节数，负数表示错误码
 ********************************************************************/
DMP_API INT32 DmpSboxWriteFile(SBOX_FILE *sbox_file, const VOID *buff, INT32 size);

/********************************************************************
 * 函 数 名:  DmpSboxSeekFile
 * 描    述:  安全存储内文件定位接口
 * 输入参数:  sbox_file     SBOX文件句柄
              pos           偏移量
 * 输出参数:  无
 * 返 回 值:  DMP_OK        成功
              其他值        失败
 ********************************************************************/
DMP_API INT32 DmpSboxSeekFile(SBOX_FILE *sbox_file, INT32 pos);

/********************************************************************
 * 函 数 名:  DmpSboxGetFilePos
 * 描    述:  安全存储内文件定位查询接口
 * 输入参数:  sbox_file     SBOX文件句柄
 * 输出参数:  无
 * 返 回 值:  当前实际游标位置
 ********************************************************************/
DMP_API INT32 DmpSboxGetFilePos(SBOX_FILE *sbox_file);

/********************************************************************
 * 函 数 名:  DmpSboxCloseFile
 * 描    述:  安全存储内文件刷盘接口
 * 输入参数:  sbox_file     SBOX文件句柄
 * 输出参数:  无
 * 返 回 值:  DMP_OK        成功
              其他值        失败
 ********************************************************************/
DMP_API INT32 DmpSboxFlushFile(SBOX_FILE *sbox_file);

/********************************************************************
 * 函 数 名:  DmpSboxCloseFile
 * 描    述:  安全存储内文件关闭接口
 * 输入参数:  sbox_file     SBOX文件句柄
 * 输出参数:  无
 * 返 回 值:  无
 ********************************************************************/
DMP_API VOID DmpSboxCloseFile(SBOX_FILE *sbox_file);

#ifdef __cplusplus
}   // end of extern "C"
#endif


#endif // __DMPSBOX__

