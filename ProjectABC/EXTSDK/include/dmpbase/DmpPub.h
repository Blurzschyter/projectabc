﻿/********************************************************************
 * Copyright (C)   Huawei Tech. Co., Ltd.
 * 文 件 名    :   DmpPub.h
 * 作    者    :   huxiaoxiang 160924
 * 配套版本    :   DMPBASE_24.0_20170118
 * 文件描述    :   公用头文件，请在每个程序文件里包含
 ********************************************************************/

#ifndef __DMPPUB__
#define __DMPPUB__

#if (defined _WIN32 || defined _WIN32_WCE || defined __WIN32__)

    #undef  WIN32
    #define WIN32

    #if (defined WINAPI_FAMILY) && (WINAPI_FAMILY == WINAPI_FAMILY_PHONE_APP)
        #undef  WIN_PHONE
        #define WIN_PHONE
    #else
        #undef  WIN_DESKTOP
        #define WIN_DESKTOP
    #endif

#endif

// Windows下DLL导出/导入便捷宏定义
#ifdef _WINDLL
#ifdef DMPBASE
#define DMP_API __declspec(dllexport)
#else
#define DMP_API __declspec(dllimport)
#endif
#else
#define DMP_API
#endif

#ifdef  __cplusplus
#define DMP_C_API extern "C" DMP_API
#else
#define DMP_C_API DMP_API
#endif

#ifdef  __cplusplus
#define DMP_C_DECLARE_BEGIN extern "C" {
#else
#define DMP_C_DECLARE_BEGIN
#endif

#ifdef  __cplusplus
#define DMP_C_DECLARE_END }
#else
#define DMP_C_DECLARE_END
#endif


#ifdef WIN32
#pragma warning(disable: 4251)
#endif
#ifdef __ANDROID_API__
#undef ANDROID
#define ANDROID
#endif

#ifdef __APPLE__
#include <TargetConditionals.h>
    #if TARGET_OS_IPHONE
        #undef  IOS
        #define IOS
    #else
        #undef  MAC
        #define MAC
    #endif
#endif

#include <math.h>
#include <errno.h>
#include <stdio.h>
#include <stdarg.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#ifdef WIN32
#include <windows.h>
#endif

#ifdef __APPLE__
#include <mach/mach.h>
#include <mach/task.h>
#endif

#if (defined ANDROID) || (defined IOS) || (defined LINUX) || (defined MAC)
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/select.h>
#include <netinet/in.h>
#include <unistd.h>
#include <fcntl.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <pthread.h>
#include <semaphore.h>
#include <errno.h>
#include <libgen.h>
#endif

// 标准STL头文件，后面的类型定义需要用到
#ifdef    __cplusplus
#include <string>
#include <list>
#include <queue>
#include <deque>
#include <set>
#include <map>
#include <vector>
#endif

//
// 公共数据类型定义
//

#ifndef CHAR
#define CHAR        char
#endif

#ifndef UCHAR
typedef unsigned char UCHAR;
#endif

#ifndef SHORT
typedef signed short  SHORT;
#endif

#ifndef USHORT
typedef unsigned short USHORT;
#endif

#ifndef LONG
#define LONG        long
#endif

#ifndef ULONG
typedef unsigned long ULONG;
#endif

#ifndef LLONG
#define LLONG   long long
#endif

#ifndef ULLONG
#define ULLONG  unsigned long long
#endif

#ifndef FLOAT
typedef float FLOAT;
#endif

#ifndef DOUBLE
typedef double DOUBLE;
#endif

#ifndef INT8
typedef signed char INT8;
#endif

#ifndef UINT8
typedef unsigned char UINT8;
#endif

#ifndef INT16
typedef signed short  INT16;
#endif

#ifndef UINT16
typedef unsigned short UINT16;
#endif

#ifndef INT32
typedef signed int INT32;
#endif

#ifndef UINT32
typedef unsigned int UINT32;
#endif

#ifndef INT64
typedef long long int INT64;
#endif

#ifndef UINT64
typedef unsigned long long int UINT64;
#endif

#ifndef VOID
#define VOID    void
#endif

#ifdef WIN32
#include <basetsd.h>
#else
#define SIZE_T size_t
#endif

#ifdef WIN32
#define SOCKLEN_T   INT32
#else
#define SOCKLEN_T   socklen_t
#endif

#ifndef STRING
#define STRING  std::string
#endif

#ifndef STRING_LIST
#define STRING_LIST std::list <std::string>
#endif

#ifndef STRING_VECTOR
#define STRING_VECTOR std::vector <std::string>
#endif

#ifndef WSTRING
#define WSTRING  std::wstring
#endif

#ifndef WSTRING_LIST
#define WSTRING_LIST std::list <std::wstring>
#endif

#ifndef WSTRING_VECTOR
#define WSTRING_VECTOR std::vector <std::wstring>
#endif

#ifndef PAIR
#define PAIR    std::pair
#endif

#ifndef SET
#define SET    std::set
#endif

#ifndef MULTISET
#define MULTISET    std::multiset
#endif

#ifndef LIST
#define LIST    std::list
#endif

#ifndef QUEUE
#define QUEUE   std::queue
#endif

#ifndef DEQUE
#define DEQUE std::deque
#endif

#ifndef MAP
#define MAP     std::map
#endif

#ifndef MULTIMAP
#define MULTIMAP     std::multimap
#endif

#ifndef VECTOR
#define VECTOR  std::vector
#endif

#ifndef NULL
#define NULL    (0)
#endif

#ifdef __APPLE__
    #ifndef OBJC_BOOL_DEFINED
    typedef signed char BOOL;
    #endif
#else
    typedef int BOOL;
#endif

#ifndef TRUE
#ifdef true
#define TRUE    true
#else
#define TRUE    1
#endif
#endif

#ifndef FALSE
#ifdef false
#define FALSE   false
#else
#define FALSE   0
#endif
#endif

#ifndef MIN
#define MIN(a,b) (((a)>(b))?(b):(a))
#endif

#ifndef MAX
#define MAX(a,b) (((a)>(b))?(a):(b))
#endif



// 公共返回值定义
#define DMP_OK              (0)
#define DMP_ERR             (-1)
#define DMP_EAGAIN          (-2) // 也可能是超时
#define DMP_EINTR           (-3)
#define DMP_ECONNRESET      (-4) // 连接被复位
#define DMP_ENOMEM          (-5)
#define DMP_EENDOFMEDIA     (-6)


// 最大域名长度
#define MAX_DOMAIN_NAME     256

// 最大文件名长度
#define MAX_FILENAME_LEN    256

// 最大文件全路径长度
#ifndef MAX_PATH_LEN
#define MAX_PATH_LEN        1024
#endif

// 最大URL长度
#define MAX_URL_LEN         4096

// 最大IPv4地址字符串长度，包括\0
#ifndef DMP_INET_ADDRSTRLEN
#define DMP_INET_ADDRSTRLEN     16
#endif

// 最大IPv6地址字符串长度，包括\0
#ifndef DMP_INET6_ADDRSTRLEN
#define DMP_INET6_ADDRSTRLEN    46
#endif

// MAC地址长度
#ifndef MAC_ADDR_LEN
#define MAC_ADDR_LEN        6
#endif

// 秒的换算单位
#define UNIT_SECOND2MS      1000

// s到us的换算
#define UINT_SECOND2USEC    1000000

// 分钟的换算单位
#define  UNIT_MIN2SEC       60

// 字节换算单位
#define  UNIT_KB            1024

// 比特换算单位
#define  UNIT_KBIT          1000

// 字节到比特的转换单位
#define  UNIT_BYTE2BIT      8

// 十六进制基数
#define  UNIT_HEX           16

// 八进制基数
#define  UNIT_OCTAL         8

// 二进制基数
#define  UNIT_BINARY        2

// 计算百分比基数
#define  UNIT_PERCENT       100

// 非法文件句柄
#define INVALID_FD          (-1)

// 非法socket句柄
#define INVALID_SOCK        (-1)

// 非法CPU标识
#define INVALID_CPU_ID      (-1)

#endif // __DMPPUB__

