/********************************************************************
 * Copyright (C), Huawei Tech. Co., Ltd.
 * 文 件 名:   DmpPlayerDef.h
 * 作    者:   qianshaobin 322591
 * 创建日期:   2015-11-27
 * 文件描述:   定义了播放器对外常量及protocol
 ********************************************************************/

#ifndef DmpPlayerDef_h
#define DmpPlayerDef_h

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

#pragma mark -
#pragma mark 类型定义
//资源类型
typedef enum{
    DmpPlayer_HLS_LIVE,         // HLS直播或时移(可判断JetPlayable的timeshift是否大于0来确定是否时移)
    DmpPlayer_HLS_VOD,          // HLS VOD
    DmpPlayer_MP4,              // MP4
    DmpPlayer_HSS_LIVE,         // HSS 直播
    DmpPlayer_HSS_VOD,          // HSS vod
    DmpPlayer_DASH_LIVE,
    DmpPlayer_DASH_VOD,
	
	//为了方便区分资源类型，建议使用以下类型，以上类型只是为了兼容以前版本，以后将不在维护
	DmpPlayer_VOD,              //点播
	DmpPlayer_LIVE,             //直播
	DmpPlayer_TSTV,             //时移
	DmpPlayer_NPVR,             //录播
}DmpPlayer_TYPE;

//video fill mode
//typedef enum {
//    VIDEO_RESIZE_ASPECT = 0,            //保持视频比例不变，画面在矩形内全部显示--默认
//    VIDEO_RESIZE_ASPECT_FILL = 1,       //保持视频比例不变，使画面充满矩形
//    VIDEO_RESIZE = 2,                   //拉伸视频比例使画面充满矩形，画面内容全部显示
//}VIDEO_FILL_MODE;

//连接状态类别
typedef enum{
    DmpPlayer_CONNECTION_DNS,         // 连接dns查询OK
    DmpPlayer_CONNECTION_REDIRECT,    // 连接重定向OK
    DmpPlayer_CONNECTION_HMS,         // 连接HMS接收数据OK
}DmpPlayer_CONNECTION_TYPE;

typedef enum {
    DmpPlayer_PLAYBACK_TYPE_PROGRAM,                //正片
    DmpPlayer_PLAYBACK_TYPE_ADVERTISE,              //广告
    //DmpPlayer_PLAYBACK_TYPE_MOSAIC,               //马赛克
    //DmpPlayer_PLAYBACK_TYPE_INDEPENDENCE,           //独立播放
}DmpPlayer_PLAYBACK_TYPE;

typedef enum {
                                         //                                         返回值
    GET_DOWNLOAD_SPEED,                  // 获取下载速度                          [NSNumber numberWithFloat:]
    GET_DOWNLOAD_SIZE,                   // 获取已下载大小，                       单位：字节（byte）[NSNumber numberWithLongLong]
    GET_MEDIA_BITRATES,                  // 获取码率列表                          NSArray  {NString}
    GET_AUDIOTRACK_LIST,                 // 获取音轨列表                          NSArray  {JetMediaOption}
    GET_SUBTITLES_LIST,                  // 获取字幕列表                          NSArray  {JetMediaOption}
    GET_CURRENT_AUDIOTRACK,        // 获取当前音轨名称
    GET_CURRENT_SUBTITLE,          // 获取当前字幕名称
    GET_CC_LANGS_LIST,              //获取CC字幕列表
    GET_CURRENT_CC_LANG,            //获取当前CC字幕序号 [NSNumber numberWithInt:]
    GET_PLAY_FASTFORWARD_ABILITY,        // 获知是否可以快进,在正常播放后调用此接口   [NSNumber numberWithBool:]
    GET_PLAY_FASTREVERSE_ABILITY,        // 获知是否可以快退,在正常播放后调用此接口   [NSNumber numberWithBool:]
    GET_LATEST_TS_REQUEST_ADDRESS,       // 获取最后一次请求分片的分片请求地址       [NString]
    GET_LIVE_ABOSOLUTE_TIME,             // 获取直播时播放器的播放绝对时间,单位:秒  [NSNumber numberWithLong:]
    
    GET_BUFFERED_LENGTH,                 // 查询ios播放器缓存长度,单位为秒,[NSNumber numberWithLong:]
    
    GET_DMPPLAYER_STATE,                 // 查询播放器状态，[NSNumber numberWithInt],DmpPlayer_state类型
                                        //0:idle state,Player has created;
                                        //1:init state,Player has been set with media source
                                        //2:preparing state,Player has been started and preparing for media data
                                        //3:buffering state,Player is receiving data
                                        //4:playing state,Playback is in progress
                                        //5:playing state,Playback is paused
                                        //6:play end state,Playback is finished
                                        //7:stop state,Player has been stopped
                                        //8:error state,Error occurs
                                        //9:unknown state
    GET_PLAYING_BANDWIDTH,              // 查询当前码率，[NSNumber numberWithInteger]
    GET_CURRENT_PLAYBACK_TYPE,          //   获取当前的播放方式，正片还是广告
    GET_DURATION,                       // 获取正片时长, [NSNumber numberWithFloat]
    GET_PROGRESS,                        // 获取进度
    
    GET_MEDIA_CODEC_TYPE,               //获取媒体编码类型          [NString]
                                        // avc  返回 "UVMOS_VIDEO_CODEC_H264"  hevc 返回 "UVMOS_VIDEO_CODEC_HEVC"  其他返回"UVMOS_VIDEO_CODEC_BUTT"
    GET_VIDEO_INFO_FPS,                 //获取视频的帧率              [NSNumber numberWithDouble] eg：25,表示一秒钟25帧
    GET_VIDEO_CURRENT_FPS,              //获取视频的当前帧率              [NSNumber numberWithDouble] eg：25,表示当前帧率一秒钟25帧
    GET_VIDEO_HEIGHT,                   //获取视频的高度               [NSNumber numberWithInteger]  单位：像素点  eg:480
    GET_VIDEO_WIDTH,                    //获取视频的宽度               [NSNumber numberWithInteger]    单位：像素点  eg:320
    GET_PLAY_REALTIME_BITRATE,          //获取实时码率            [NSNumber numberWithInteger]
    GET_PLAYER_COMMENTS,                //获取指定播放器的备注信息，未指定plyaerID时返回当前播放器的备注信息, [NString]
    GET_SMPTE_TT_LIST,                   // 获取SMPTE-TT字幕列表                  NSArray  {JetMediaOption}
    GET_CURRENT_SMPTE_TT,                // 获取当前SMPTE-TT字幕名称，NSString，为nil表示SMPTE-TT字幕关闭。
	GET_LAST_ERROR_HOST,                //获取最后出错地址（域名/IP地址）,NSString
    GET_QP_FOR_UVMOS,                   //获取QP值                [NSNumber numberWithInteger],(返回值范围0-51,未解析QP值为0。实际范围1-51)
	GET_ACCESS_DELAY,                   //连接建立的时间点查询，即startUpEvent中最后一个事件的时间点,[NSNumber numberWithLongLong:]
	GET_STALLING_TRACK,                 //【本节目的】的卡顿记录，格式：时间点，卡顿时长 ;时间点为相对StartTime的秒数，卡顿时长单位为毫秒，小于200毫秒的卡顿不记录。多次卡顿分号分隔，最多记录20次。5,2543;12,1528;312,8457
    GET_PLAY_SESSIONID,                 //获取本次播放唯一对应的SessionID，该SessionID用于唯一标识本次播放
}GET_PROPERTY_KEY;


typedef enum {
                                         //                 参数
    SET_MANUAL_BITRATE,                  // 设置码率立即切换的码率值，NSString, -1为最低码率，－2为最高码率，0为自适应码率，大于0为真实码率值
    SET_BANDWIDTH_SWITCH_SMOOTH,         // 设置码率平滑切换的码率值，NSString, -1为最低码率，－2为最高码率，0为自适应码率，大于0为真实码率值
    SET_AUDIOTACK,                       // 设置音轨     NSString, 音轨名称
    
    SET_SUBTITLE,                        // 设置字幕     NSString，字幕名称
    SET_CLOSE_CAPTION,                   // 设置CC字幕开关   [NSNumber numberWithbool:]
    SET_AUDIO_PREFERED_LANG,             // 设置偏好音轨语言  NSString （播放器初始化之后调用）,需要设置3位的语言码,如"eng,chi,msa"偏好语言以","分割，最多可设置三个参数，如果不设置则默认选择第一路播放
    SET_SUBTITLE_PREFERED_LANG,          // 设置偏好字幕语言  NSString （播放器初始化之后调用）,需要设置3位的语言码，如"eng,chi,msa"，偏好字幕以","分割，最多可设置三个参数，如果不设置则默认选择第一路播放
    SET_SUBTITLE_DEFAULT_ONOFF,          // 设置字幕默认开/关 [NSNumber numberWithbool:] 1为开，0为关，默认为开。
    SET_NTP_DIFF_TIME,                    // 设置NTP时间差值，即UTC时间与本地时间（设备时间）的差值，单位为秒，当前默认为东八区时间（值 -8*3600）
                                         //                [NSNumber numberWithLong:];
    //SET_ADD_BLACK_LAYER,                //添加黑布
    //SET_REMOVE_BLACK_LAYER              //移除黑布
    SET_LOCAL_CACHE,                       // 设置PE本地缓冲打开，[NSNumber numberWithbool],1表示开启，0表示关闭，系统默认关闭；本接口已无效。
    SET_LOCAL_CACHE_PARAM,                 // 设置播放器本地缓冲参数，DmpLocalCache 类型；本接口已无效。
    SET_LOCAL_CACHE_THREAD_NUM,            // 设置本地缓存下载线程数。[NSNumber numberWithInteger]；本接口已无效。
    SET_LOCAL_CACHE_DISABLED,              // 关闭本地缓存， NSString；本接口已无效
    SET_AUDIO_FADE_OUT,                    // 设置音频淡出时间，单位mS，[NSNumber numberWithInteger]，系统默认为0
    SET_LIVE_PLAYLIST_SIZE_LIMIT,          // 设置直播ts分片个数,[NSNumber numberWithInteger],默认全部下载
    SET_VOLUME,                            // 设置音量，DmpVolume类型，音量范围：[0, 10000]系统默认采用最大音量播放，注：此音量与系统音量不相同
    SET_NETWORK_SUSPEND,                   // 设置网络挂起
    SET_NETWORK_RESUME,                    // 网络恢复，NSString类型服务地址,为空表示在原网络内恢复，不为空表示切换到其他网络后网络恢复
    SET_DOLBY_DAA_END_POINT,               // 设置杜比音效输出终端设备，[NSNumber numberWithInteger],0表示手机，1表示耳机
    SET_DOLBY_DAA_DAP_ONOFF,               // 设置杜比音效开启或关闭，[NSNumber numberWithInteger],1表示开启，0表示关闭
    SET_DOLBY_DAA_DIALOG_ENHANCEMENT,      // 设置杜比音效效果，[NSNumber numberWithInteger],0为关闭，1为正常增强，2为中级增强，3为高级增强
    SET_CONNECTION_IS_LONG,                // 设置网络连接类型是否为长连接，[NSNumber numberWithInteger], 0为不是长连接(短连接)，1为是长连接，默认为1.
    SET_BUFFERING_LENGTH_TO_PLAY,          // 设置开始播放需要缓冲的时长（单位：毫秒）,[NSNumber numberWithInteger], 最小不能低于800ms, 最大不能大于10000ms, 默认为800ms。
    SET_PLAYER_COMMENTS,                   // 设置指定播放器的备注信息，未指定plyaerID时，设置当前播放器的备注信息, [NString]
    SET_AUTO_ADAPT_BY_CPU,                 // 设置是否支持性能自适应，默认不支持，1为支持，0为不支持，[NSNumber numberWithbool:]
    SET_HTTP_CONNECT_NUMBER,               // 设置3TCP特性的连接数，默认为3，有效的范围为1－5，[NSNumber numberWithInteger:]
    SET_HTTP_CONNECT_RANGE_SIZE,           // 设置3TCP特性的连接请求长度，默认为512000bytes，有效的范围为200000bytes－700000bytes，[NSNumber numberWithInteger:]
    SET_CONFIG_BUFFERING_TIME_LIMIT,       // 设置缓冲区大小，单位s,有效的范围暂定为40s - 200s，[NSNumber numberWithInteger:]
    SET_SMPTE_TT,                          //设置SMPTE-TT字幕，NSString, nil或空字符串@""为关闭字幕。设置SMPTE-TT字幕会关闭普通字幕和CC字幕，但关闭SMPTE-TT字幕不会打开普通字幕和CC字幕。
	SET_CC_LANG,                           //设置CC字幕，[NSNumber numberWithInteger:]
    SET_REQUEST_COOKIE,                   //设置向服务器请求时的cookie参数，NSString，nil或空字符串@""会默认设成默认值,默认由播放器自动设置。
    SET_REQUEST_USER_AGENT,               //设置向服务器请求时的user－agent参数，NSString，nil或空字符串@""会默认设成默认值,默认由播放器自动设置。
    SET_ASPECT_RATIO,                       //设置宽高比，如果是4：3，设置值为1.33，如果是16：9，就设置1.77 [NSNumber numberWithFloat:]
    
    SET_ASPECT_RATIO_FILL,                  //设置是否铺满整个播放窗口，1为铺满，0为不铺满，不铺满后默认展示源的宽高 [NSNumber numberWithbool:]
    
    SET_IPV6_IS_ENABLED,                    //设置是否开启ipv6功能    [NSNumber numberWithbool:]
	
	SET_X_ONLINE_HOST,                     //设置扩展头域 用于私有网关协议X-ONLINE-HOST
    
    SET_DRM_MEDIA_ID,                       //设置meidiaid
    SET_QP_FOR_UVMOS,                       //设置是否需要解析QP值,[NSNumber numberWithInteger],0为不解析,1为解析，默认是0.
    SET_NO_CACHE,                           //设置请求时是否添加no-cache字段，该字段用于向不支持HLS／DASH协议cache的服务器请求（支持cache时设置为NO；不支持cache时设置为YES）。默认为NO。为YES时，播放器请求非点播片源的索引文件时，将在请求中添加no-cache字段。该接口只在播放前设置才有效。[NSNumber numberWithbool:]
    SET_DECODE_TYPE,                        //设置解码类型，[NSNumber numberWithbool:]， 0为使用软解码，1为优先使用硬解码，默认为1。
    SET_VR_MODE,                            //设置VR模式，[NSNumber numberWithInteger]， 0为关闭VR模式，1为单目VR模式，默认为0。
    SET_VR_FINGER_ROTATION,                 //设置VR模式下的触屏旋转角度信息，NSString,格式为"x,y", x和y为浮点数
    SET_SUBSCRIBERID,                       //设置用户识别号
    SET_PHYSICALDEVICEID,                   //设置物理id
    SET_CONTENTCODE,                        //设置设备型号
    SET_PLAY_BUFFER_PARA,                   //设置起播缓冲参数
    SET_PLAY_SPEED,                         //设置播放速度
}SET_PROPERTY_KEY;

//错误类型
typedef enum{
    DmpPlayer_ERROR_DRM_ERROR,              // DRM error
    DmpPlayer_ERROR_START_ERROR,            // 播放未开始出错
    DmpPlayer_ERROR_INTERRUPT_ERROR,        // 播放过程中出错
    DmpPlayer_ERROR_WARNING,                // 播放告警
    DmpPlayer_ERROR_SQM_FROZEN_PICTURE,     // SQM frozen picture信息上报。
}DmpPlayer_ERROR_TYPE;

typedef enum{
	DmpPlayer_EVENT_BITRATE_SWITCH_EVENT,     // 码率切换事件上报。
	DmpPlayer_EVENT_SMPTETT_DETECTED_EVENT,   // 检测到smptett字幕的事件上报
	DmpPlayer_EVENT_CC_DETECTED_EVENT,        // 检测到cc字幕的事件上报
    DmpPlayer_EVENT_SWITCH_DECODER_EVENT,     // 播放器从硬解码切换到软解码通知(硬解码不支持)
    DmpPlayer_EVENT_CDN_SWITCH_EVENT,         // CDN切换事件上报
}DmpPlayer_EVENT_TYPE;

typedef enum {
    BYTES_RECEIVED,                     //已接受字节数               返回值:[NSNumber numberWithInt:]
    CHANNE_CHANGE_TIME,                 //频道切换时间               返回值:[NSNumber nsnumberWithDouble:]
    SPECIFIED_DURATION,                 //内容时长                  返回值:[NSNumber numberWithInt:] 单位：秒
    NR_OF_CONTENT_PROFILES,             //码率个数                  返回值:[NSNumber numberWithInt:]
    NR_OF_REDIRECTS,                    //重定向次数                返回值:[NSNumber numberWithInt:]
    ORIGINATING_SOURCE,                 //原始播放地址               返回值:NSString
    ORIGINATING_SERVER_MANIFEST,        //最终播放地址               返回值:NSString
    PLAYLIST_TYPE,                      //播放列表类型               返回值:NSString
    MANIFEST_FILE,                      //原始播放列表文件内容         返回值:NSString
    ORIGINATING_SERVER_SEGMENT,         //首分片真实URI              返回值:NSString
    SEGMENT_PROFILE_NR,                 //当前视频码率编号            返回值:[NSNumber numberWithInt:]
    MANIFEST_SOURCE_IP,                 //播放列表获取IP              返回值:NSString
    SEGMENT_SOURCE_IP,                  //分片文件获取IP              返回值:NSString
    NR_OF_REQUEST_ERRORS,                //HTTP 请求失败次数          返回值:[NSNumber numberWithInt:]
    NR_OF_REQUESTS,                      //HTTP请求次数               返回值:[NSNumber numberWithInt:]
    LOW_DOWNLOAD_SPEED_TS_NUMBER,        //下载码率低于码率的分片个数    返回值:[NSNumber numberWithInt:]
    SEGMENT_PROFILE_BITRATE,             //当前播放码率                返回值:NSString   0表示是自适应码率
    SEGMENT_READ_BITRATE,                //当前下载码率                返回值:[NSNumber numberWithLonglong:]
    NR_OF_SEGMENTS_REQUESTED,            //分片请求总数                返回值:[NSNumber numberWithInt:]
    NR_OF_SEGMENTS_RECEIVED              //分片已接受总数              返回值:[NSNumber numberWithInt:]
}GET_AGAMA_SQM;


typedef enum{
    /* for PlayReady */
    SET_DRM_ADDITIONAL_HEADER,          // additional header string
    SET_DRM_CUSTOM_HEADER,              // custom data string
    SET_DRM_SERVER,                      // drm server url string
    SET_DRM_PR_WHITE_LIST,               //playReady white list
    
    /* for VMX */
    SET_DRM_VMX_SERVER_URL,             //VMX服务器地址（IP:PORT）， NSString
    SET_DRM_VMX_COMPANY_NAME,           //VMX加密使用的company name, NSString
    SET_DRM_VMX_LOG_LEVEL,              //VMX加密模块的日志级别, 0:关闭日志，1:error级别，2:info级别，3:debug级别，4:trace级别,默认为debug级别, [NSNumber numberWithInteger:],
    
    
    /*for novel ca*/
    SET_DRM_NOVEL_URL,
    SET_DRM_NOVEL_DATA
}SET_DRM_KEY;

typedef struct VMX_PARAS
{
	char *serverUrl;
	char *companyName;
	int logLevel;
}VMX_PARAS;

typedef struct PLAYREADY_PARAS
{
	char *serverUrl;
	char *customHeader;
	char *AdditionalHeader;
	char *whiteList;
}PLAYREADY_PARAS;

typedef union DRM_UNION{
	VMX_PARAS vmxParas;
	
	PLAYREADY_PARAS prParas;
}DRM_UNION;

/* for SQM */

typedef enum {
    SQM_STATISTICS_CYCLE,               //获取sqm统计周期            返回值:[NSNumber numberWithInt:]
    DOWNLOADED_SLICE_NUMBER,            //已下载的分片数量           返回值:NSString 逗号分割
    EXPECTED_DOWNLOAD_SLICE_NUMBER,     //期望下载的分片数量         返回值:NSString 逗号分割
    DOWNLOAD_INTERRUPT_SLICE_NUMBER,    //下载中断的分片数量         返回值:NSString 逗号分割
    DON_NOT_DOWNLOAD_SLICE_NUMBER,      //不可下载的分片数量         返回值:NSString 逗号分割
    BITRATE_REDUCE_NUMBER,              //码率下切的次数            返回值:[NSNumber numberWithInt:]
    BITRATE_INCREASE_NUMBER,            //码率上切的次数            返回值:[NSNumber numberWithInt:]
    LAST_SLICE_IP_ADDRESS,              //本周期内下载的最后一个分片IP地址     返回值:NSString
	LAST_SLICE_PORT,                    //本周期内下载的最后一个分片端口号     返回值:int
    PROFILE_STREAM_NUMBER,              //profile区间时延           返回值:NSString 逗号分割
    BLOCKED_STREAM_NUMBER,              //时间区间时延               返回值:NSString 逗号分割
    FROZEN_PIC_TRIGER,                  //FrozenPicture的阈值，单位：秒。连续N秒no data，则次数加1     返回值:[NSNumber numberWithInt:]
    ARTEFACT_TRIGER,                    //Artefact的阈值，单位：秒。连续N秒with Errors,次数加1        返回值:[NSNumber numberWithInt:]
    LIPSYNC_TRIGER,                     //LipSync的阈值,单位：秒。连续N秒LipSync，次数加1             返回值:[NSNumber numberWithIt:]
	THRESHOLD_PROGRAMDURATION,          // 获取节目播放时长阈值，对于统计时长（有MOS值的秒数）小于阈值Device.X_DT.Threshold.ProgramDuration的ProgramPeriodic 和ProgramInfo都 不上报（避免因频繁换台导致数据太多）
    SOURCE_IP,                          //错误来源Ip，               返回值：NSString
    SECONDS_WITH_NO_DATA_HISTOGRAM,     //断流秒数直方图             返回值：NSString  分号分割
    MEDIA_CHANGED_DELAY_HISTOGRAM,      //频道切换延迟直方图          返回值: NSString  分号分割
    REPORT_LOG_SWITCH,                  //report日志上报开关         返回值：[NSNumber numberWithInt:]
    ERROR_LOG_SWITCH,                   //error日志上报开关          返回值：[NSNumber numberWithInt:]
    CRASH_LOG_SWITCH,                   //crash日志上报开关          返回值：[NSNumber numberWithInt:]
    SUBSCIBER_SWITCH,                   //用户级别开关               返回值：[NSNumber numberWithInt:]
    DEVICE_SWITCH,                      //设备级别开关               返回值：[NSNumber numberWithInt:]
    REPORT_PARAM,                       //report需要上报的参数       返回值:NSString 逗号分割,如果没有，则代表上传所有参数
    ERROR_PARAM,                        //error需要上报的参数        返回值：NSString 逗号分割，如果没有，则代表上传所有参数
    SECONDS_WITHOUT_ERROR,              //无错播放总时长，单位：秒     返回值:[NSNumber numberWithInt:]
    
    REALTIME_MOS_VAL,                               //mos实时值的直方图  MOS在各个区间的秒数  NSString
    CPU_USAGE,                                      //CPU使用率直方图  NSString
    RAM_USAGE,                                      //RAM使用率直方图   NSString
    SIGNAL_STRENGTH,                                //信号强度值直方图   输入: 信号强度值，类型为FLOAT    输出: 直方图字符串，已逗号分开。类型为NSString
    DOWNLOAD_SPEED_HISTOGRAM,                       //下载速度直方图  输出: 直方图字符串，已逗号分开。类型为NSString
    KEY_UP_BANDWIDTH,                                // 上行带宽直方图   输出: 直方图字符串，已逗号分开。类型为NSString
    KEY_DOWN_BANDWIDTH,                             //下行带宽直方图     输出: 直方图字符串，已逗号分开。类型为NSString
    KEY_INDEX_DELAY,                                //索引文件响应时间直方图 输出: 直方图字符串，已逗号分开。类型为NSString
    KEY_CHUNK_DELAY,                                //分片响应时间直方图 直方图字符串，已逗号分开。类型为NSString
    KEY_HTTP_DELAY,                                 //HTTP响应时间直方图 输出: 直方图字符串，已逗号分开。类型为NSString
    KEY_AVERAGE_DOWNLOAD_SPEED_VALUE,               //统计周期内平均下载速度   输出: 平均下载速度  [NSNumber numberWithInt:]
    KEY_UVMOS,                                      //UVMOS平均得分 [NSNumber numberWithDouble:]
    
    SQM_INDEX_NUM,                                  //索引(如M3U8/MPD)请求总次数    [NSNumber numberWithInt:]
    SQM_INDEX_SUCCESS_NUM,                          //索引(如M3U8/MPD)请求成功次数，2xx和3xx算成功，4xx和5xx，以及超时无响应算失败 [NSNumber numberWithInt:]
    SQM_INDEX_DELAY_AVG,                            //索引(如M3U8/MPD)响应平均延时（单位：毫秒）    [NSNumber numberWithInt:]
	SQM_CHUNK_REQUEST_NUM,                          //分片请求总次数    [NSNumber numberWithInt:]
	SQM_CHUNK_REQUEST_SUCCESS_NUM,                  //分片请求成功次数 [NSNumber numberWithInt:]
    SQM_CHUNK_DELAY_AVG,                            //分片响应平均延时（单位：毫秒）   [NSNumber numberWithInt:]
    
    SQM_PROFILE_PLAYOUT_SEC_HISTOGRAM,              //每个码率的节目播放的时长，播放时间直方图 单位：秒。按Profile统计。 NSString
    SQM_SQUALITY_HISTOGRAM,                          //sQuality在各个区间的秒数 NSString
    SQM_SVIEW_HISTOGRAM,                            //sView在各个区间的秒数。 NSString
    SQM_SINTERACTION_HISTOGRAM,                     //sInteraction在各个区间的秒数 NSString
    
    SQM_BITRATE,                                    //平均码率  [NSNumber numberWithInt:],还未实现
    SQM_REDIRECT_TIMES,                             //播放请求重定向次数  [NSNumber numberWithInt:]
    SQM_DL_SPEED_HISTOGRAM,                   //每个分片计算一个下载速率，然后统计下载速率在各个区间的个数 NSString
	
	SQM_REALTIME_SWITCH,                            //获取实时上报开关 [NSNumber numberWithInt:]
	SQM_ALARM_SWITCH,                               //获取告警上报开关  [NSNumber numberWithInt:]
	SQM_MOS_TRIGER,									//播放质量差时长，即MOS值低于阈值的时长，单位：秒
	REALTIME_HAS_INFO,                              //实时上报HAS信息 NSString
	KEY_RECEIVER_IP,                                //RECEIVER_IP      NSString
	KEY_RECEIVER_PORT,                              //RECEIVER_PORT   [NSNumber numberWithInt:]
	
	GET_HOMEPAGE_DELAY_HISTOGRAM,                 //epg首页响应延时直方图        NSString
	GET_TVGUIDE_DELAY_HISTOGRAM,                  //tvguide 页面响应时延直方图   NSString
	GET_VODLIST_DELAY_HISTOGRAM,                  //vod列表请求延时直方图        NSString
	GET_VODDETAIL_DELAY_HISTOGRAM,                //vod详情页面延时直方图        NSString
	GET_SEARCH_DELAY_HISTOGRAM,                  //search响应延时直方图         NSString
	RECDATION_DELAY_HISTOGRAM,                   //recommendation响应延时直方图  NSString
	START_UP_EVENT,                              //起播播放事件日志，格式为：eventCode(事件时刻点，ip地址，结果码)。事件时刻点格式：1970年到现在的毫秒数，如果是DNS，正确填0，有错误填错误码，如果没有具体错误码，可填-1，如果是HTTP，有响应码填响应码，没有填-1
	
	SQM_ERROR_INTERVAL,                  //错误上报间隔  [NSNumber numberWithInt:]
	SQM_ERROR_CACHE_NUM,                 //错误缓存个数  [NSNumber numberWithInt:]
    SQM_REG_RESPONSE_PARA,               //注册回应消息内容  NSString
    
    OFFLINE_TOTAL_SQM_DATA,             //离线下载SQM数据汇总         返回值: NSString Json字符串
    OFFLINE_DOWNLOADED_SLICE_NUMBER,            //离线下载中已经收到的离线分片的直方图    不直接返回，从OFFLINE_TOTAL_SQM_DATA中解析
    OFFLINE_EXPECTED_DOWNLOAD_SLICE_NUMBER,     //离线下载中期望收到的离线分片的直方图    不直接返回，从OFFLINE_TOTAL_SQM_DATA中解析
    OFFLINE_DOWNLOAD_INTERRUPT_SLICE_NUMBER,    //离线下载中被取消的离线分片的直方图      不直接返回，从OFFLINE_TOTAL_SQM_DATA中解析
    OFFLINE_DON_NOT_DOWNLOAD_SLICE_NUMBER,      //离线下载中不存在的离线分片的直方图      不直接返回，从OFFLINE_TOTAL_SQM_DATA中解析
//    OFFLINE_DOWNLOAD_UNIQUE_DOWNLOAD,           //离线下载中新增的下载节目数量          返回值：[NSNumber numberWithInt:]
//    OFFLINE_DOWNLOAD_SUCCESS,                   //离线下载中下载成功的节目数量          返回值：[NSNumber numberWithInt:]
//    OFFLINE_DOWNLOAD_ABORTED,                   //离线下载中下载中断的节目数量          返回值：[NSNumber numberWithInt:]
//    OFFLINE_DOWNLOAD_NON_AVALIABLE,             //离线下载中不可下载的节目数量          返回值：[NSNumber numberWithInt:]
//    OFFLINE_DOWNLOAD_IN_COMPLETE,               //离线下载中正在下载的节目数量          返回值：[NSNumber numberWithInt:]
//    OFFLINE_DOWNLOAD_SOURCE,                    //离线下载中最后一次请求的IP地址        返回值: NSString
//    OFFLINE_DOWNLOAD_AVG_SPEED,                 //离线下载中下载的平均速度,单位bps      返回值：[NSNumber numberWithInt:]
}GET_SQM_PARAM;


typedef enum {
    SET_USER_NAME,                      //用户名                   参数：NSStrig
    SET_USER_PASSWORD,                  //密码                     参数：NSString
    SET_HTTPS_CERT_FULLPATH,            //https校验证书路径          参数：NSString
    SET_NEED_HTTPS,                     //是否需要https证书认证      参数:[NSNumber numberWithBool:]
    SET_NEED_HTTPS_CERT,                //是否需要https需要校验证书   参数：[NSNumber numberWithBool:]
    SET_NEED_DIGEST_AUTH,               //是否使用摘要认证            参数:参数：[NSNumber numberWithBool:]
    SET_SUBSCIBER_SWITCH,               //用户级别开关               参数：[NSNumber numberWithBool:]
    SET_MEDIA_CHANGE_DELAY,             //流切换时延                 参数：[NSNumber numberWithInt:]  单位：毫秒(ms)
	SET_SIGNAL_STRENGTH,                //设置信号强度               参数：[NSNumber numberWithfloat:]
	SET_HOMEPAGE_DELAY,                 //epg首页响应延时时间         参数：[NSNumber numberWithInt:] 单位：毫秒（ms）
	SET_TVGUIDE_DELAY,                  //tvguide 页面响应时延时间    参数：[NSNumber numberWithInt:] 单位：毫秒（ms）
	SET_VODLIST_DELAY,                  //vod列表请求延时时间          参数：[NSNumber numberWithInt:] 单位：毫秒（ms）
	SET_VODDETAIL_DELAY,                //vod详情页面延时时间          参数：[NSNumber numberWithInt:] 单位：毫秒（ms）
	SET_SEARCH_DELAY,                  //search响应延时时间           参数：[NSNumber numberWithInt:] 单位：毫秒（ms）
	SET_UP_BANDWIDTH,                   // 上行带宽               参数：[NSNumber numberWithLong:] 单位：kbps
	SET_DOWN_BANDWIDTH,                 //下行带宽				 参数：[NSNumber numberWithLong:] 单位：kbps
	SET_CPU_USAGE,                      //设置cpu             参数：[NSNumber numberWithInt:]
	SET_RAM_USAGE,                      //设置ram             参数：[NSNumber numberWithInt:]
    SET_SESSIONID,                      //设置SESSIONID              参数：NSString
    SET_TOKEN,                          //设置TOKEN                  参数：NSString
    SET_DEVICEID,                       //设置DEVICEID               参数：NSString
    SET_SUBSCRIBEID,                    //设置SUBSCRIBEID            参数：NSString
    SET_DISPATCH_PARA                   //设置调度参数接口             参数：NSString
}SET_SQM_PARAM;

typedef enum {
    FP_USERID,                          // 用户ID,作为是否启用FP功能的开关,用户ID为空FP功能关闭   参数：NSString
    FP_DISPLAY_INTERVAL,                // 显示间隔,单位:秒 ,默认300秒                 参数：[NSNumber numberWithInt:]
    FP_DISPLAY_TIME,                    // 显示时间,单位:秒，默认10秒钟,允许设置0.1秒    参数：[NSNumber numberWithFloat:]
    FP_USERID_COLOR,                    // 用户ID字体颜色,默认为红色     参数：UIColor类型
    FP_USERID_FONT,                     // 用户ID字体大小，默认为17      参数：[NSNumber numberWithInt:]
    FP_BACKGROUNDCOLOR,                 // FP背景色,默认为白色          参数：UIColor类型
}FINGERPRINTINGPROPERTY_KEY;

typedef enum {
    CA_TYPE_IS_VMX,                        //VMX加密
    CA_TYPE_IS_PLAYREADY,                  //Playready加密
    CA_TYPE_IS_NOVEL,                      //Novel加密
	CA_TYPE_IS_NONE,					   //非加密流，清流
}CA_TYPE;

typedef struct DmpVolume
{
    int         mute;       ///< a flag indicating whether audio output is mute(1) or not(0)
    int         volume;     ///< audio volume
} DmpVolume;  //only used by PE

typedef struct DmpLocalCache
{
    int                 cacheTime;           ///< time to cache in local file in seconds
    char                *cacheDir;           ///< file path to cache the data
} DmpLocalCache;

typedef enum
{
    /**
     * @brief   idle state
     *
     * @note    Player has created.
     */
    DmpPlayer_STATE_IDLE = 0,
    
    /**
     * @brief   init state
     *
     * @note    Player has been set with media source.
     */
    DmpPlayer_STATE_INIT,
    
    /**
     * @brief   preparing state
     *
     * @note    Player has been started and preparing for media data.
     */
    DmpPlayer_STATE_PREPARING,
    
    /**
     * @brief   buffering state
     *
     * @note    Player is receiving data.
     */
    DmpPlayer_STATE_BUFFERING,
    
    /**
     * @brief   playing state
     *
     * @note    Playback is in progress.
     */
    DmpPlayer_STATE_PLAYING,
    
    /**
     * @brief   playing state
     *
     * @note    Playback is paused.
     */
    DmpPlayer_STATE_PAUSE,
    
    /**
     * @brief   play end state
     *
     * @note    Playback is finished.
     */
    DmpPlayer_STATE_PLAYEND,
    
    /**
     * @brief   stop state
     *
     * @note    Player has been stopped.
     */
    DmpPlayer_STATE_STOP,
    
    /**
     * @brief   error state
     *
     * @note    Error occurs.
     */
    DmpPlayer_STATE_ERROR,
    
    /**
     * @brief   unknown state
     */
    DmpPlayer_STATE_UNKNOWN
} DmpPlayer_State;

typedef struct {
	void *player;                            // 播放器实例
	NSUInteger playerID;                  // 播放器id
	DmpPlayer_PLAYBACK_TYPE playbackType; // 播放器内容类型
}DmpPlayer_Info;

// 可播放的对象,实现此协议的对象都可以作为播放器的输入对象
@protocol JetPlayable <NSObject>
-(NSString*) url;                       // index.m3u8的地址
-(CFTimeInterval) timeshift;            //（只适用于直播） 时移秒数，0表示不支持
-(DmpPlayer_TYPE) type;                 // 此节目的类型
@end

@protocol JetErrorInfo <NSObject>
-(NSString*) errorName;                       // 错误编码
-(DmpPlayer_ERROR_TYPE) errorType;            // 错误类型
@end

typedef struct {
    int  serverWeight;                   //表示权重
    int  clusterLevel;                   //标识优先级
    char *cdnId;                         //标识一个cdn厂商
    char *serverId;                      //唯一标识一个服务器
    char *clusterId;                     //唯一标识一个集群
    char *cdnUrl;                        //HMS的播放地址或者第三方CDN的播放地址（只提供ip或者域名）
}CDN_Info;
typedef struct {
    char *DrmType;                       //Drm类型，如果UI传空，默认为PLAYREADY_CLIENT
    char *deviceType;                    //设备类型。如STB，ANDROID_PHONE,ANDROID_PHONE,IPAD,IPHONE
    char *serverUrl;                     //设备证书服务器地址
    char *additionalHeader;              //如Cooki
    char *networkDeviceId;               //对应于登录认证请求中上报给平台的physicalDeviceDId
    char *jSessionId;                    //UI登录认证返回的jSessionId
}DrmCertReq;
#endif /* DmpPlayerDef.h */
