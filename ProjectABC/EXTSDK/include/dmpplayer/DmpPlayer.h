/********************************************************************
 * Copyright (C), Huawei Tech. Co., Ltd.
 * 文 件 名:   DmpPlayer.h
 * 作    者:   DmpPlayer
 * 创建日期:   2013-05-02
 * 文件描述:   定义了播放器对外接口 包含接口协议，类型定义，错误上报通知，播放器功能接口
 ********************************************************************/

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "DmpPlayerDef.h"

#pragma mark 字幕音轨
@interface JetMediaOption : NSObject
@property(nonatomic, copy)NSString *optionLocale;             // 字幕或多音轨的语言(通过中间件查询对应的映射用于展示)
@property(nonatomic, assign)NSInteger index;                  // 数组中的index(用于设置字幕音轨)，注意:值不一定是连续的0-n
@end

#pragma mark -
#pragma mark 播放器状态通知接口
@class DmpPlayer;
@protocol DmpPlayerDelegate <NSObject>
@optional

// 节目加载完成时触发
// 注意, onLoad时在4.3上duration还是不可用的, 要通过onDurationChanged来获取时长的变化
-(void) onLoad:(CFTimeInterval)duration initalprogress:(CFTimeInterval)initialProgress player:(DmpPlayer*)player playerID:(NSUInteger)playerID playbackType:(DmpPlayer_PLAYBACK_TYPE)playbackType;

// 缓冲状态和非缓冲状态之间转换时触发
// 缓冲状态下,需要显示缓冲图标, 非缓冲状态下,应该隐藏缓冲图标
// 建议：缓冲状态下，应避免切换字幕音轨等动作
-(void) onBuffering:(BOOL)buffering player:(DmpPlayer *)player playerID:(NSUInteger)playerID playbackType:(DmpPlayer_PLAYBACK_TYPE)playbackType;

// 缓冲时长变更时触发
// 缓冲未完成状态下,需要显示缓冲图标, 缓冲完成状态下,应该隐藏缓冲图标。bufferingRate为缓冲完成百分比，即缓冲开始时为0%，缓冲完成时为100%
-(void) onBufferingUpdate:(float)bufferingRate player:(DmpPlayer *)player playerID:(NSUInteger)playerID playbackType:(DmpPlayer_PLAYBACK_TYPE)playbackType;

// 当播放进度改变时触发，progress 播放进度，单位为s
// 对于vod节目,返回和设置秒数(正数),
// 对于时移,返回和设置相对直播时间的秒数(负数),当前时间即直播时间为原点0,对纯直播无效(返回nan).
// 如果获取进度失败也会返回nan
-(void) onProgress:(CFTimeInterval)progress player:(DmpPlayer *)player playerID:(NSUInteger)playerID playbackType:(DmpPlayer_PLAYBACK_TYPE)playbackType;

// 此方法会在时长变的时候通知插件,4.3上onLoad时duration还是不可用的,要用这个校正
// 注意seek时可能会触发这个通知
// ios6.1以上版本可能会收不到此通知
-(void) onDurationChanged:(CFTimeInterval)duration player:(DmpPlayer *)player playerID:(NSUInteger)playerID playbackType:(DmpPlayer_PLAYBACK_TYPE)playbackType;

// 节目正常播放完成时触发
-(void) onComplete:(DmpPlayer *)player playerID:(NSUInteger)playerID playbackType:(DmpPlayer_PLAYBACK_TYPE)playbackType;

// 节目被关闭或替换时触发
-(void) onUnload:(DmpPlayer *)player playerID:(NSUInteger)playerID playbackType:(DmpPlayer_PLAYBACK_TYPE)playbackType;

// 从暂停状态到播放状态
-(void) onPlay:(DmpPlayer *)player playerID:(NSUInteger)playerID playbackType:(DmpPlayer_PLAYBACK_TYPE)playbackType;

// 从播放状态到暂停状态
-(void) onPause:(DmpPlayer *)player playerID:(NSUInteger)playerID playbackType:(DmpPlayer_PLAYBACK_TYPE)playbackType;

// 出错通知，errCode包含了原有的epp vmx错误通知
-(void) onError:(id<JetErrorInfo>)errInfo player:(DmpPlayer *)player playerID:(NSUInteger)playerID playbackType:(DmpPlayer_PLAYBACK_TYPE)playbackType;

//SQM事件上报
-(void) onSqmEvent:(NSString*)eventCode eventInfo:(id)info playerID:(NSUInteger)playerID playbackType:(DmpPlayer_PLAYBACK_TYPE)playbackType;

// 连接通知
-(void) onConnection:(DmpPlayer_CONNECTION_TYPE)connType player:(DmpPlayer *)player playerID:(NSUInteger)playerID playbackType:(DmpPlayer_PLAYBACK_TYPE)playbackType;

//EIT信息上报
- (void) onParentControl:(NSInteger)controlCode
               eventId:(NSInteger)eventID
             eventName:(NSString *)name
      eventDescription:(NSString *)descriptions
             startTime:(long long int)startTime
              duration:(unsigned long long int)duration
                player:(DmpPlayer *)player
                playerID:(NSUInteger)playerID
            playbackType:(DmpPlayer_PLAYBACK_TYPE)playbackType;

//blackOut上报
- (void) OnBlackOut:(BOOL)isOnblackOut player:(DmpPlayer *)player playerID:(NSUInteger)playerID playbackType:(DmpPlayer_PLAYBACK_TYPE)playbackType;

-(void)onNotification:(DmpPlayer_EVENT_TYPE)eventType eventInfo:(id)info playerInfo:(DmpPlayer_Info)playerInfo;

/* 以下为可能出现的错误码
    //EPP错误
    107801	获取片源index.m3u8时，hms返回超时，如果有rss，经过rss容灾后也超时,无其他报错的缓冲超时	
    107802	获取片源index.m3u8时，hms报400以上错误
    107803	获取所有playlist内容，hms都返回超时
    107804	获取所有playlist内容，hms都有错误，最后一个playlist返回400以上错误
    107805	获取ts文件超时	获取ts文件超时
    107806	获取ts文件报400以上错误	获取ts文件报400以上错误，且播放器状态异常，上报
    107807	直播媒体服务器playlist不更新
    107808  当前下载速率低于片源最低码率的要求时会上报
    107813  本地无网络的情况下会上报，包括本地无WIFI网络和对端服务器端口未侦听这两种情况
	107815	下载播放列表RRS服务器返回400+错误
	107816	网络正常，RRS服务器无响应，下载master playlist超时
	107817	网络异常，终端无法连接到RRS服务器
	107818	网络异常，终端无法连接到HMS服务器
	107819	设置了码率过滤后playlist过滤后无可播放码率
	107822	从时移HMS下载二级播放列表HMS服务器返回400+错误
	107823	从时移HMS下载媒体分片HMS服务器返回400+错误
 
    //播放器错误
    107809  播放器加载失败
    107810  播放器进度未更新错误
    107811	缓冲超时报错
    107812	iOS系统不支持
	107011	越狱设备上报
	107012	处于调试状态的告警或报错
	SQM_FROZEN_PICTURE	片源播放时出现卡顿时上报
 
	107901	PE内部错误
	107902	media解密失败
	107903	切音轨、字幕、码率失败
	107904	直播playlist不更新
	107905	流媒体格式不支持
	107906	播放器创建音频解码器失败
	107907	播放器创建视频解码器失败
	107908	播放器创建字幕解码器失败
	107911	播放列表中音视频长度不一致导致SEEK到无效分片
	107912	播放列表解析失败
	107913	视频分辨率不支持
	107914	媒体分片封装格式不支持
	107915	视频编码格式不支持
	107916	音频编码格式不支持
	107917	字幕编码格式不支持
	107922	内存不足
 
    //HSS错误
    106701  播放器内部出错
    106702  解析错误
    106703  网络错误
    106704  playready模块错误
    106705  未知错误
 
    //CA(PlayReady, VMX)错误
    105401  一般错误
    105402  安全错误
    105403  系统时间异常
    105404  无连接
    105405  过期
    105406  获取证书失败
    105407  证书无效
    105408  解密失败
    105409  没有权限
    105410  离线播放错误
    105411  证书地址不合法错误
    105412  模块不支持错误
    105413  地域限制错误
    105414  输出控制错误,不允许外放
    105415  超出设备限制数量
    105417  设备被越狱
    105418  url错误
    105419  服务器响应错误
    105420  读写文件出错（比如，设置的日志文件路径没有权限)
    105421  无效公司名
 
     Note: 107805、107806、107808、107820、107823、SQM_FROZEN_PICTURE 属于提示性错误，在一定程度上影响用户体验（e.出现缓冲）,不需要强制关闭播放器(现已不上报)。
           其它错误为播放器不可恢复的错误，后续无法在进行正常播放，需要关闭或者重启播放器。
 */
@end


@interface DmpPlayer : NSObject

// 播放器delegate，用于播放器状态通知
@property(nonatomic, assign)id<DmpPlayerDelegate> delegate;

// 播放器可操作性，由于播放器的加载 seek等操作都是异步的，而且播放器处理请求时不接受其他请求
//@property(nonatomic, readonly)BOOL operable;

// 查看播放器是否在播放中
@property(nonatomic, readonly)BOOL playing;

// 播放器视图,可用于设置frame 背景图等等
// removeFromSuperview时会同时释放播放器
// 使用时请不要将指针指向nil
@property(nonatomic, readonly, getter = getCurrentPlayerView)UIView *view;

#pragma mark -
#pragma mark  版本信息获取
/********************************************************************
 * 函 数 名:  getDmpPlayerVersion
 * 描    述:  版本号获取
 * 输入参数:  无
 * 输出参数:  无
 * 返 回 值:  NSString 播放器版本号
 ********************************************************************/
+ (NSString *)getDmpPlayerVersion;

#pragma mark -
#pragma mark  获取唯一设备Id
/********************************************************************
 * 函 数 名:   getSecureUniqueID
 * 描    述:  通过CA获取唯一设备ID
 * 输入参数:  无
 * 输出参数:  无
 * 返 回 值:  NSString 设备ID
 ********************************************************************/
+ (NSString *)getSecureUniqueID:(CA_TYPE)catype;

#pragma mark -
#pragma mark  播放器初始化及设置
/********************************************************************
 * 函 数 名:  initPlayerWithFrame:
 * 描    述:  初始化播放器
 * 输入参数:  frame: 播放器frame
 * 输出参数:  无
 * 返 回 值:  id DmpPlayer 实例
 ********************************************************************/
- (id)initPlayerWithFrame:(CGRect)frame;

/********************************************************************
 * 函 数 名:  enableCAServer:serverPort:serverName:
 * 描    述:  开启并设置CA加密(内部创建子线程进行认证)
 * 输入参数:  serverAddr：加密服务器地址
 port：端口号
 serverName:加密服务提供公司名
 * 输出参数:  无
 * 返 回 值:  void
 ********************************************************************/
+ (void)enableCAServer:(NSString *)serverAddr serverPort:(NSString *)port serverName:(NSString *)name;

/********************************************************************
 * 函 数 名:  enableCAServer:serverPort:serverName:
 * 描    述:  开启并设置CA加密(内部创建子线程进行认证)
 * 输入参数:  serverAddr：加密服务器地址
 port：端口号
 serverName:加密服务提供公司名
 delegate : 实现DmpPlayerDelegate协议的类
 * 输出参数:  无
 * 返 回 值:  void
 ********************************************************************/
+ (void)enableCAServer:(NSString *)serverAddr serverPort:(NSString *)port serverName:(NSString *)name withDelegate:(id)delegate;

/********************************************************************
 * 函 数 名:  setPlayerVideoFillMode:
 * 描    述:  设置播放器的显示bounds
 * 输入参数:  mode 播放器视频适配比
 * 输出参数:  无
 * 返 回 值:  void
 ********************************************************************/
//- (void)setPlayerVideoFillMode:(VIDEO_FILL_MODE)mode;

#pragma mark -
#pragma mark 播控功能(单例,当前播放器，正片)
/********************************************************************
 * 函 数 名:  load:bookmark:startPlay:
 * 描    述:  加载播放器资源
 * 输入参数:  newProgram: 实现JetPlayable协议的节目资源
 bookMark: 书签，单位为s
 play： 是否加载完播放
 * 输出参数:  无
 * 返 回 值:  void
 ********************************************************************/
- (void)load:(id <JetPlayable>)newProgram bookMark:(CFTimeInterval)bookMark startPlay:(BOOL)play;

/********************************************************************
 * 函 数 名:  play
 * 描    述:  播放
 * 输入参数:  无
 * 输出参数:  无
 * 返 回 值:  void
 ********************************************************************/
- (void)play;

/********************************************************************
 * 函 数 名:  setPlayRate:
 * 描    述:  快进或快退播放， 
              快进：  GET_PLAY_FASTFORWARD_ABILITY 返回值为YES时，可设置速率>2.0
                    在ios7上，若返回值为NO，可设置速率1.0~2.0
              快退： rate < 0.0f
              rate = 0.f暂停
              rate = 1.0f 恢复正常播放
              rate = 2.0f 快进2倍数
              rate = 4.0f 快进4倍数
              rate = -1.0f 快退1倍数
              rate = -2.0f 快退2倍数
 * 输入参数:  rate
 * 输出参数:  无
 * 返 回 值:  void
 ********************************************************************/
- (void)setPlayRate:(float)rate;

/********************************************************************
 * 函 数 名:  pause
 * 描    述:  暂停
 * 输入参数:  无
 * 输出参数:  void
 * 返 回 值:  无
 ********************************************************************/
- (void)pause;

/********************************************************************
 * 函 数 名:  seek
 * 描    述:  播放
 * 输入参数:  无
 * 输出参数:  无
 * 返 回 值:  void
 ********************************************************************/
- (void)seek:(float)seekTime;

/********************************************************************
 * 函 数 名:  stop
 * 描    述:  关闭播放器，
 * 输入参数:  无
 * 输出参数:  无
 * 返 回 值:  void
 ********************************************************************/
- (void)stop;

/********************************************************************
 * 函 数 名:  removeDmpPlayer
 * 描    述:  移除所有播放器view和控制模块
 * 输入参数:  无
 * 输出参数:  无
 * 返 回 值:  id
 ********************************************************************/
-(void)removeDmpPlayer;

/********************************************************************
 * 函 数 名:  reOpen
 * 描    述:  重新打开播放器
 * 输入参数:  无
 * 输出参数:  无
 * 返 回 值:  void
 ********************************************************************/
- (void)reOpen;

//设置正片是否静音
- (void) setMute:(BOOL) isMute;

/********************************************************************
 * 函 数 名:  getPlaybackType:
 * 描    述:  获取当前播放模式
 * 输入参数:  无
 * 输出参数:  无
 * 返 回 值:  id
 ********************************************************************/
- (DmpPlayer_PLAYBACK_TYPE)getPlaybackType;

/********************************************************************
 * 函 数 名:  getProperty:
 * 描    述:  根据key获取属性,获取当前正片的属性
 * 输入参数:   GET_PROPERTY_KEY 属性名称
 * 输出参数:  无
 * 返 回 值:  id，根据不同的属性，返回的类型会有所不同，请参考GET_PROPERTY_KEY中具体的属性描述
 ********************************************************************/
- (id)getProperty:(GET_PROPERTY_KEY)key;

/********************************************************************
 * 函 数 名:  setProperty:withValue:
 * 描    述:  根据key设置当前正片的属性
 * 输入参数:  SET_PROPERTY_KEY 属性名称
 id
 * 输出参数:  无
 * 返 回 值:  id
 ********************************************************************/
- (BOOL)setProperty:(SET_PROPERTY_KEY)key withValue:(id)value;

/********************************************************************
 * 函 数 名:  setBitrateFilter:imaxBitrate
 * 描    述:  过滤码率，操作对象为当前播放器实例
 * 输入参数:  minRate：最小码率  maxBitrate:最大码率
 * 输出参数:  无
 * 返 回 值:  无
 ********************************************************************/
- (void)setBitrateFilter:(int)minBitRate imaxBitrate:(int)maxBitRate;

/********************************************************************
 * 函 数 名:  getAgamaSQM:
 * 描    述: 获取agama 的 sqm参数
 * 输入参数:  GET_AGAMA_SQM
 * 输出参数:  无
 * 返 回 值:  id 返回参数类型见GET_AGAMA_SQM定义说明
 ********************************************************************/
- (id) getAgamaSQM:(GET_AGAMA_SQM)para;

/********************************************************************
 * 函 数 名:  getFingerPrintingProperty:withValue:
 * 描    述:  根据key获取FingerPrinting属性
 * 输入参数:  无
 * 输出参数:  无
 * 返 回 值:  id
 ********************************************************************/
- (id)getFingerPrintingProperty:(FINGERPRINTINGPROPERTY_KEY)key;

/********************************************************************
 * 函 数 名:  setFingerPrintingProperty:withValue:
 * 描    述:  根据key设置FingerPrinting属性
 * 输入参数:  无
 * 输出参数:  无
 * 返 回 值:  id
 ********************************************************************/
- (BOOL)setFingerPrintingProperty:(FINGERPRINTINGPROPERTY_KEY)key withValue:(id)value;

/********************************************************************
 * 函 数 名:  startShowFingerPrinting:
 * 描    述:   根据传入的userId开始showFingerPrinting
 * 输入参数:  NSString
 * 输出参数:  无
 * 返 回 值:  id
 ********************************************************************/
-(void)startShowFingerPrinting:(NSString *)userId;

/********************************************************************
 * 函 数 名:  addBlackOut
 * 描    述:  使播放器黑屏
 * 输入参数:  无
 * 输出参数:  无
 * 返 回 值:  无
 ********************************************************************/
- (void) addBlackOut;

/********************************************************************
 * 函 数 名:  removeBlackOut
 * 描    述:  移除播放器的黑屏
 * 输入参数:  无
 * 输出参数:  无
 * 返 回 值:  无
 ********************************************************************/
- (void) removeBlackOut;

- (void)screenShotWithCompletionHandler:(void (^)(UIImage* image))completionHandler;

#pragma mark -
#pragma mark 播控功能（多实例）
/********************************************************************
 * 函 数 名:  createPlayerInstanceInPlaybackType: withFrame:
 * 描    述:  创建播放器实例
 * 输入参数:  playbackType: 播放器类型。DmpPlayer_PLAYBACK_TYPE_PROGRAM正片播放器只能有1个。
 *           frame: 视图位置

 * 输出参数:  无
 * 返 回 值:  播放器ID，目前限制创建播放器实例数为16个(1-16)。如果返回值为0，表示创建播放器实例失败。
 ********************************************************************/
-(NSUInteger)createPlayerInstanceInPlaybackType:(DmpPlayer_PLAYBACK_TYPE)playbackType withFrame:(CGRect)frame;

/********************************************************************
 * 函 数 名:  load:bookmark:startPlay:
 * 描    述:  加载播放器资源
 * 输入参数:  newProgram: 实现JetPlayable协议的节目资源
 bookMark: 书签，单位为s
 play： 是否加载完播放
 * 输出参数:  无
 * 返 回 值:  void
 ********************************************************************/
- (void)load:(id <JetPlayable>)newProgram bookMark:(CFTimeInterval)bookMark startPlay:(BOOL)play inPlayerID:(NSUInteger)playerID;

/********************************************************************
 * 函 数 名:  play
 * 描    述:  播放
 * 输入参数:  无
 * 输出参数:  无
 * 返 回 值:  void
 ********************************************************************/
- (void)play:(NSUInteger)playerID;

/********************************************************************
 * 函 数 名:  setPlayRate:
 * 描    述:  快进或快退播放，
 快进：  GET_PLAY_FASTFORWARD_ABILITY 返回值为YES时，可设置速率>2.0
 在ios7上，若返回值为NO，可设置速率1.0~2.0
 快退： rate < 0.0f
 rate = 0.f暂停
 rate = 1.0f 恢复正常播放
 * 输入参数:  rate
 * 输出参数:  无
 * 返 回 值:  void
 ********************************************************************/
- (void)setPlayRate:(float)rate inPlayerID:(NSUInteger)playerID;

/********************************************************************
 * 函 数 名:  pause
 * 描    述:  暂停
 * 输入参数:  无
 * 输出参数:  void
 * 返 回 值:  无
 ********************************************************************/
- (void)pause:(NSUInteger)playerID;

/********************************************************************
 * 函 数 名:  seek
 * 描    述:  播放
 * 输入参数:  无
 * 输出参数:  无
 * 返 回 值:  void
 ********************************************************************/
- (void)seek:(float)seekTime inPlayerID:(NSUInteger)playerID;

/********************************************************************
 * 函 数 名:  stop
 * 描    述:  关闭播放器，
 * 输入参数:  无
 * 输出参数:  无
 * 返 回 值:  void
 ********************************************************************/
- (void)stop:(NSUInteger)playerID;

/********************************************************************
 * 函 数 名:  removeDmpPlayer
 * 描    述:  移除指定播放器view和控制模块
 * 输入参数:  无
 * 输出参数:  无
 * 返 回 值:  id
 ********************************************************************/
-(void)removeDmpPlayer:(NSUInteger)playerID;

/********************************************************************
 * 函 数 名:  reOpen
 * 描    述:  重新打开播放器，保留目前播放状态
 * 输入参数:  无
 * 输出参数:  无
 * 返 回 值:  void
 ********************************************************************/
- (void)reOpen:(NSUInteger)playerID;

//设置正片是否静音
- (void) setMute:(BOOL) isMute inPlayerID:(NSUInteger)playerID;

-(UIView*)getPlayerView:(NSUInteger) playerID;

/********************************************************************
 * 函 数 名:  getProperty:
 * 描    述:  根据key获取属性,获取当前正片的属性
 * 输入参数:   GET_PROPERTY_KEY 属性名称
 * 输出参数:  无
 * 返 回 值:  id，根据不同的属性，返回的类型会有所不同，请参考GET_PROPERTY_KEY中具体的属性描述
 ********************************************************************/
- (id)getProperty:(GET_PROPERTY_KEY)key inPlayerID:(NSUInteger)playerID;

/********************************************************************
 * 函 数 名:  setProperty:withValue:
 * 描    述:  根据key设置当前正片的属性
 * 输入参数:  SET_PROPERTY_KEY 属性名称
 id
 * 输出参数:  无
 * 返 回 值:  id
 ********************************************************************/
- (BOOL)setProperty:(SET_PROPERTY_KEY)key withValue:(id)value inPlayerID:(NSUInteger)playerID;

/********************************************************************
 * 函 数 名:  setBitrateFilter:imaxBitrate: inPlayerID:
 * 描    述:  过滤码率,指定播放器实例进行码率过滤
 * 输入参数:  minRate：最小码率  maxBitrate:最大码率
 * 输出参数:  无
 * 返 回 值:  无
 ********************************************************************/
- (void)setBitrateFilter:(int)minBitRate imaxBitrate:(int)maxBitRate inPlayerID:(NSUInteger)playerID;

/********************************************************************
 * 函 数 名:  getFingerPrintingProperty:withValue:
 * 描    述:  根据key获取FingerPrinting属性
 * 输入参数:  无
 * 输出参数:  无
 * 返 回 值:  id
 ********************************************************************/
- (id)getFingerPrintingProperty:(FINGERPRINTINGPROPERTY_KEY)key inPlayerID:(NSUInteger)playerID;

/********************************************************************
 * 函 数 名:  setFingerPrintingProperty:withValue:
 * 描    述:  根据key设置FingerPrinting属性
 * 输入参数:  无
 * 输出参数:  无
 * 返 回 值:  id
 ********************************************************************/
- (BOOL)setFingerPrintingProperty:(FINGERPRINTINGPROPERTY_KEY)key withValue:(id)value inPlayerID:(NSUInteger)playerID;

/********************************************************************
 * 函 数 名:  addBlackOut
 * 描    述:  使播放器黑屏
 * 输入参数:  无
 * 输出参数:  无
 * 返 回 值:  无
 ********************************************************************/
- (void) addBlackOut:(NSUInteger)playerID;

/********************************************************************
 * 函 数 名:  removeBlackOut
 * 描    述:  移除播放器的黑屏
 * 输入参数:  无
 * 输出参数:  无
 * 返 回 值:  无
 ********************************************************************/
- (void) removeBlackOut:(NSUInteger)playerID;

/********************************************************************
 * 函 数 名:  getPlaybackType:
 * 描    述:  切换至正指定的播放器
 * 输入参数:  无
 * 输出参数:  无
 * 返 回 值:  id
 ********************************************************************/
- (DmpPlayer_PLAYBACK_TYPE)getPlaybackType:(NSUInteger)playerID;

/********************************************************************
 * 函 数 名:  switchPlayer:
 * 描    述:  切换至正指定的播放器
 * 输入参数:  无
 * 输出参数:  无
 * 返 回 值:  id
 ********************************************************************/
- (BOOL)switchPlayer:(NSUInteger)playerID;

/********************************************************************
 * 函 数 名:  getCurrentPlayerID
 * 描    述:  获取当前播放实例的playerID
 * 输入参数:  无
 * 输出参数:  无
 * 返 回 值: NSUInterger，未设置当前播放器实例时返回0。设置了当前播放器实例时，返回播放器实例的playerID
 ********************************************************************/
- (NSUInteger)getCurrentPlayerID;

- (void)screenShotWithCompletionHandler:(void (^)(UIImage* image))completionHandler inPlayerID: (NSUInteger)playerID;

/********************************************************************
 * 函 数 名:  getPlayerList:
 * 描    述:  获取所有播放器的列表（playerID）
 * 输入参数:  filterJson, 过滤条件的json字符串，样例如下：
 {
 "FilterInfo" : {
 "playerID":1,
 "playbackType":1,
 “playerInfo":comments"
 }
 }
 * "playerID":1,表示playerID == 1
 * "playbackType":1, 表示playbacktype == 1
 * “playerInfo":comments",表示条件 [playerInfo containsString:[comments lowercaseString]];
 *playerInfo的过滤条件为包含,即playerInfo为"1comments"或"comments2"或“3comments4”都符合这个过滤条件
 *filterJson为nil或过滤条件为空时，输出全部播放器列表
 *所有过滤条件为&&(与)的关系，
 * 输出参数:  无
 * 返 回 值:  id， 输出playerID的数组,例如{1,2,5}
 ********************************************************************/
- (id)getPlayerList:(NSString*)filterJson;

//#pragma mark -
//#pragma mark 广告播控功能
/********************************************************************
 * 函 数 名:  load:bookmark:startPlay:
 * 描    述:  加载播放器资源
 * 输入参数:  newProgram: 实现JetPlayable协议的节目资源
 bookMark: 书签，单位为s
 play： 是否加载完播放
 * 输出参数:  无
 * 返 回 值:  void
 ********************************************************************/
//- (void)loadAd:(id <JetPlayable>)newProgram bookMark:(CFTimeInterval)bookMark startPlay:(BOOL)play;

/********************************************************************
 * 函 数 名:  play
 * 描    述:  播放
 * 输入参数:  无
 * 输出参数:  无
 * 返 回 值:  void
 ********************************************************************/
//- (void)playAd;

/********************************************************************
 * 函 数 名:  setPlayRate:
 * 描    述:  快进或快退播放，
 快进：  GET_PLAY_FASTFORWARD_ABILITY 返回值为YES时，可设置速率>2.0
 在ios7上，若返回值为NO，可设置速率1.0~2.0
 快退： rate < 0.0f
 rate = 0.f暂停
 rate = 1.0f 恢复正常播放
 * 输入参数:  rate
 * 输出参数:  无
 * 返 回 值:  void
 ********************************************************************/
//- (void)setAdPlayRate:(float)rate;

/********************************************************************
 * 函 数 名:  pause
 * 描    述:  暂停
 * 输入参数:  无
 * 输出参数:  void
 * 返 回 值:  无
 ********************************************************************/
//- (void)pauseAd;

/********************************************************************
 * 函 数 名:  seek
 * 描    述:  播放
 * 输入参数:  无
 * 输出参数:  无
 * 返 回 值:  BOOL 是否可seek
 ********************************************************************/
//- (BOOL)seekAd:(float)seekTime;

/********************************************************************
 * 函 数 名:  stop
 * 描    述:  关闭播放器，
 * 输入参数:  无
 * 输出参数:  无
 * 返 回 值:  void
 ********************************************************************/
//- (void)stopAd;

/********************************************************************
 * 函 数 名:  reOpen
 * 描    述:  重新打开播放器，保留目前播放状态
 * 输入参数:  无
 * 输出参数:  无
 * 返 回 值:  void
 ********************************************************************/
//- (void)reOpenAd;

//设置广告是否静音
//- (void) setMuteAd:(BOOL) isMute;

/********************************************************************
 * 函 数 名:  getAdProperty:
 * 描    述:  根据key获取当前广告属性
 * 输入参数:  无
 * 输出参数:  无
 * 返 回 值:  id
 ********************************************************************/
//- (id)getAdProperty:(GET_PROPERTY_KEY)key;

/********************************************************************
 * 函 数 名:  setAdProperty:withValue:
 * 描    述:  根据key设置当前广告属性
 * 输入参数:  无
 * 输出参数:  无
 * 返 回 值:  id
 ********************************************************************/
//- (BOOL)setAdProperty:(SET_PROPERTY_KEY)key withValue:(id)value;

/********************************************************************
 * 函 数 名:  switchPlaybackType:
 * 描    述:  切换至正片播放或广告播放
 * 输入参数:  无
 * 输出参数:  无
 * 返 回 值:  id
 ********************************************************************/
//- (BOOL)switchPlaybackType:(DmpPlayer_PLAYBACK_TYPE)playbackType;

#pragma mark -
#pragma mark  VMX CA

#pragma mark  PlayReady安全

/********************************************************************
 * 函 数 名:  PlayReadyGetRootLicense
 * 描    述:  获取根证书
 * 输入参数:  serverUrl: 根证书服务器地址
 serviceID: serviceID
 cdata : customdata
 * 输出参数:  无
 * 返 回 值:  0:success        other:fail
 ********************************************************************/
+ (int)PlayReadyGetRootLicense:(char *)serverUrl dsId:(char *)dsId serviceId:(char *)serviceId cdata:(char *)data;

/********************************************************************
 * 函 数 名:  PlayReadyJoinDomain
 * 描    述:加入域
 * 输入参数:
 *  	   serverUrl: 域服务器地址
 *         serviceId: service id
 *         cdata: customdata
 * 输出参数:  无
 * 返 回 值:  0:success           other:fail
 ********************************************************************/
+ (int)PlayReadyJoinDomain:(char *)serverUrl keyId:(char *)keyId cdata:(char *)data;

#pragma mark
#pragma mark  SQM日志上报接口
/********************************************************************
 * 函 数 名:  SqmInitJson:serverUrl:callback:
 * 描    述:sqm注册设备
 * 输入参数:
 *  	   SqmInitJson: 注册需要参数组成的json字符串
 *         serverUrl: SQM服务器地址
 *         callback:异步回调，返回初始化结果，YES，成功；NO，失败.初始化成功后才可以设置、获取参数
 * 输出参数:  无
 * 返 回 值: 无
 ********************************************************************/
+(void)sqmInitJson:(NSString *)jsonNs serverUrl:(NSString *)initServerUrl callback:(void (^)(BOOL isSucceed))callback;

/********************************************************************
 * 函 数 名:  getParamThroughSQM
 * 描    述:  获取SQM参数接口
 * 输入参数:  GET_SQM_PARAM 枚举值
 * 输出参数:  无
 * 返 回 值:  id 返回参数类型见GET_SQM_PARAM定义说明
 ********************************************************************/
+ (id) getParamThroughSQM:(GET_SQM_PARAM)param;

/********************************************************************
 * 函 数 名:  setSqmParam:value:
 * 描    述:  设置SQM参数接口
 * 输入参数:  SET_SQM_PARAM 枚举值
 * 输出参数:  无
 * 返 回 值:  无
 ********************************************************************/
+ (void) setSqmParam:(SET_SQM_PARAM)param value:(id)value;

/********************************************************************
 * 函 数 名:  RegisterChannelId:
 * 描    述:注册通道消息
 * 输入参数: 注册通道消息的json字符串
 * 输出参数:  无
 * 返 回 值:  0:success           other:fail
 ********************************************************************/
+(int)registerChannelId:(NSString *)jsonNs;

/********************************************************************
 * 函 数 名:  uploadSqmError:
 * 描    述:上报error类型日志
 * 输入参数: error日志的json字符串
 * 输出参数:  无
 * 返 回 值:  0:success           other:fail
 ********************************************************************/
+(int)uploadSqmError:(NSString *)jsonNs;

/********************************************************************
 * 函 数 名:  uploadSqmError:
 * 描    述:上报crash类型日志
 * 输入参数: crash日志的json字符串
 * 输出参数:  无
 * 返 回 值:  0:success           other:fail
 ********************************************************************/
+(int)uploadSqmCrash:(NSString *)jsonNs;

/********************************************************************
 * 函 数 名:  uploadSqmReport:
 * 描    述:上报report类型日志
 * 输入参数: report日志的json字符串
 * 输出参数:  无
 * 返 回 值:  0:success           other:fail
 ********************************************************************/
+(int)uploadSqmReport:(NSString *)jsonNs;

/********************************************************************
 * 函 数 名:  uploadSqmRealtime:
 * 描    述:上报实时消息类型日志
 * 输入参数: realtime日志的json字符串
 * 输出参数:  无
 * 返 回 值:  0:success           other:fail
 ********************************************************************/
+(int)uploadSqmRealtime:(NSString *)jsonNs;

/********************************************************************
 * 函 数 名:  uploadSqmAlarm:
 * 描    述:上报告警类型日志
 * 输入参数: alarm日志的json字符串
 * 输出参数:  无
 * 返 回 值:  0:success           other:fail
 ********************************************************************/
+(int)uploadSqmAlarm:(NSString *)jsonNs;

/********************************************************************
 * 函 数 名:  uploadSqmAlarmClear:
 * 描    述:上报告警消除类型日志
 * 输入参数: 告警消除日志的json字符串
 * 输出参数:  无
 * 返 回 值:  0:success           other:fail
 ********************************************************************/
+(int)uploadSqmAlarmClear:(NSString *)jsonNs;

/********************************************************************
 * 函 数 名:  uploadSqmConnectReq:
 * 描    述:上传连接请求通道
 * 输入参数: 连接请求通道的json字符串
 * 输出参数:  无
 * 返 回 值:  0:success           other:fail
 ********************************************************************/
+(int)uploadSqmConnectReq:(NSString *)jsonNs;

/********************************************************************
 * 函 数 名:  uploadSqmPara:
 * 描    述:  上报sqm参数
 * 输入参数:   要上报参数的json字符串
 * 输出参数:   无
 * 返 回 值:   0:success           other:fail
 ********************************************************************/
+(int)uploadSqmPara:(NSString *)jsonNs;

/********************************************************************
 * 函 数 名:  releaseSqm:
 * 描    述: 释放SQM
 * 输入参数:  无
 * 输出参数:  无
 * 返 回 值: 无
 ********************************************************************/
+(void)releaseSqm;

#pragma mark
#pragma mark  UvMos接口
/********************************************************************
 * 函 数 名:  sqmUvMosReg:
 * 描    述: 注册uvmos
 * 输入参数:  mediainfo:媒体信息
 * 输出参数:  无
 * 返 回 值: bool
 
 MediaInfo的Json字符窜key和value说明
 key	                       必选/可选	    类型  	         说明
 MediaType	                       M	    int	          目前取值为0，代表是VOD，后续可能有扩展
 ContentProvider	               M	    int	          目前取值为0，代表视频内容采用开源ffmpeg编码，后续可能有扩展
 CodecType	                       M	    int	          视频流的编码格式，0代表h264，1代表h265,2代表VP9
 ScreenSize	                       M	    double	      屏幕的尺寸，单位为英寸
 VideoResolutionHeight    	       M	    int	          视频分辨率高度
 VideoResolutionWidth	           M	    int	          视频分辨率宽度
 ScreenResolutionWidth	           M	    int	          屏幕分辨率宽度
 ScreenResolutionHeight	           M	    int	          屏幕分辨率高度
 
 ********************************************************************/
+(BOOL)sqmUvMosReg:(NSString*)mediaInfo;

/********************************************************************
 * 函 数 名:  sqmUvMosUnReg:
 * 描    述: 注销uvmos
 * 输入参数:  无
 * 输出参数:  无
 * 返 回 值: int 0成功，其他失败
 ********************************************************************/
+(int)sqmUvMosUnReg;

/********************************************************************
 * 函 数 名:  sqmUvMosResetStun:
 * 描    述: uvmos重置
 * 输入参数:  无
 * 输出参数:  无
 * 返 回 值: 无
 ********************************************************************/
+(void)sqmUvMosResetStun;

/********************************************************************
 * 函 数 名:  sqmUvMosCalSegment:
 * 描    述: 计算UvMos实时得分
 * 输入参数:  seqment_info 分片信息
 * 输出参数:  无
 * 返 回 值: UvMos实时得分字符窜
 
 SegmentInfo的Json字符窜的key和value说明
 key	         必选/可选	类型	         说明
 CurVideoWidth	     M	    int	     当前视频分辨率宽度
 CurVideoHeight    	 M	    int	     当前视频分辨率高度
 TimeStamp	         M	    int	     视频片段截止时间戳，以视频开始加载时为开始时间点，单位毫秒(ms)
 PlayStatus  	     M	    int	     0：播放状态，视频画面正常
 1：视频正在进行初始化缓冲
 2：视频初始化缓冲结束 (关键状态)
 3：视频画面开始出现损伤 (关键状态)，对于OTT来说就是进入卡顿
 4：视频画面持续损伤中，卡顿
 5：视频画面损伤结束，恢复正常 (关键状态)
 6：(点播类型)开始进行SEEK操作 (关键状态)
 7：正在进行SEEK操作
 8：(点播类型)SEEK操作结束，恢复正常播放 (关键状态)
 FrameRate  	     M	    double	 视频帧率
 AvgVideoBitrate	 M	    int	     视频的码率
 FrameQPValue	     M	    Double	 VBR场景，输入视频帧级QP值。直接填0即可
 
 uvmos 实时得分字符串key和value说明
 key	         必选/可选	类型	         说明
 QualitySession	    M	   double	视频已播放部分，视频质量得分    [0.0, 5.0]
 InteractionSession	M	   double	视频已播放部分，交互体验得分    [0.0, 5.0]
 ViewSession	    M	   double	视频已播放部分，观看体验得分  [0.0, 5.0]
 UvmosSession  	    M	   double	视频已播放部分，UvMOS综合得分    [1.0, 5.0]
 QualityInstant    	M	   double	当前视频片段，视频质量得分        [0.0, 5.0](计算视频片段的uvmos得分才会输出)
 InteractionInstant	M	   double	当前视频片段，交互体验得分        [0.0, 5.0] (计算视频片段的uvmos得分才会输出)
 ViewInstant	    M	   double	当前视频片段，观看体验得分        [0.0, 5.0] (计算视频片段的uvmos得分才会输出)
 UvmosInstant	    M	   double	当前视频片段，UvMOS综合得分    [1.0, 5.0] (计算视频片段的uvmos得分才会输出)
 
 ********************************************************************/
+(NSString*)sqmUvMosCalSegment:(NSString*)segment_info;

/********************************************************************
 * 函 数 名:  sqmUvMosCalcNetWorkPlan:
 * 描    述: 计算UvMos网络损耗  static_info uvmos输入参数
 * 输入参数:  mediainfo 媒体信息
 * 输出参数:  无
 * 返 回 值: UvMos长期得分字符窜
 
 static_info 视频统计信息字符窜的Json字符窜的key和value说明
 key	             必选/可选	类型	         说明
 ImpairmentDuration	    M	    int	     视频播放期间，画面损伤总时长，单位毫秒(ms)
 ImpairmentFrequency	M	    int	     视频播放期间，画面损伤次数，包括卡顿、花屏
 InitBufferLatency	    M	    int	     初始缓冲时长，单位毫秒(ms)，包括初始加载(点播)、频道切换(直播)
 VideoPlayDuration 	    M	    int	     视频可播放时长，单位秒(s)，不包含初始缓冲时间和画面卡顿时间，包括画面花屏时间
 FrameRate  	        M	    double	 视频帧率
 AvgVideoBitrate	    M	    int	     视频的码率
 
 ********************************************************************/
+(NSString*)sqmUvMosCalcNetWorkPlan:(NSString*)meida_info statisticInfo:(NSString*)static_info;


#pragma mark
#pragma mark  下载相关接口
/********************************************************************
 * 函 数 名:  GetStreamDownloadInfo:
 * 描    述: 获取流下载需要的信息（码率，时长，下载大小，分辨率）
 * 输入参数:  url:流的URL地址
             iTimeout: 超时时间（秒）
 * 输出参数:  无
 * 返 回 值:  如果获取失败，返回值为nil；获取成功，返回下载信息的json字符串，、
 样例如下：
 {
 "MediaInfo" : [
 {“StreamBitrate”:”9200”,”StreamDuration”:”7200”,”StreamSize”:”1024000”, ”StreamResoultion”:”700x600”},
 {“StreamBitrate”:”7200”,”StreamDuration”:”7200”,”StreamSize”:”824000”, ”StreamResoultion”:”600x500”},
 {“StreamBitrate”:”6200”,”StreamDuration”:”7200”,”StreamSize”:”624000”, ”StreamResoultion”:”500x400”},
 ]
 }
 ********************************************************************/
+(NSString*)GetStreamDownloadInfo:(NSString*) url timeout:(NSInteger) iTimeout;


#pragma mark -
#pragma mark   加密接口
/********************************************************************
 * 函 数 名:  setDRMProperty:
 * 描    述:  根据key设置播放器CA相关属性,需要在创建了dmpplayer之后设置，只支持单实例播放
 * 输入参数:  无
 * 输出参数:  无
 * 返 回 值:  BOOL
 ********************************************************************/
+ (BOOL)setDRMProperty:(SET_DRM_KEY)key withValue:(id)value;

/********************************************************************
 * 函 数 名: setDRMType:
 * 描    述: 支持多实例播放（vmx不支持多实例），根据ca类型设置ca相关属性，vmx需要在dmpplayer创建之前设置，其他ca类型需要在
			dmpplayer创建之后设置
 * 输入参数:  对vmx：参数player和playerID为空，对其他ca类型不能为空
 * 输出参数:  无
 * 返 回 值:  BOOL
 ********************************************************************/
+ (BOOL)setDRMType:(CA_TYPE)type withParas:(DRM_UNION)paras inPlayerID:(NSUInteger)playerID;

/********************************************************************
 * 函 数 名:  RegisterBootServer:
 * 描    述:  注册ca服务器，一定要在设置参数之后再注册
 * 输入参数:  无
 * 输出参数:  无
 * 返 回 值:  void
 ********************************************************************/
//- (void)RegisterBootServer;


#pragma mark -
#pragma mark   dmpbase接口
/********************************************************************
 * 函 数 名:  setCaCertPath:
 * 描    述:  设置CA Certificate证书地址
 * 输入参数:  path：Bundle证书的本地地址
 * 输出参数:  无
 * 返 回 值:  无
 ********************************************************************/
+ (void)setCaCertPath:(NSString*)path;

/********************************************************************
 * 函 数 名:  cloudLicenseInit: additional_header:
 * 描    述:  初始化License云请求
 * 输入参数:  license_url：License服务器地址 additional_header：额外的http请求头信息
 * 输出参数:  无
 * 返 回 值:  无
 ********************************************************************/
+ (void)cloudLicenseInit:(NSString*)license_url additional_header:(NSString*)additional_header;
/********************************************************************
 * 函 数 名:  setDrmCertReqParam:(DrmCertReq)param
 * 描    述:  设置Drm的设备证书请求参数。
 * 输入参数:  DrmCertReq请求结构体
 * 输出参数:  无
 * 返 回 值:  void
 ********************************************************************/
+ (void)setDrmCertReqParam:(DrmCertReq)param;
/********************************************************************
 * 函 数 名:  setAlgParam:(NSString*)algPara
 * 描    述:  设置播放器算法参数。
 * 输入参数:  统计得到算法参数信息
 * 输出参数:  无
 * 返 回 值:  void
 ********************************************************************/
+ (void)setAlgParam:(NSString*)algPara;
@end
